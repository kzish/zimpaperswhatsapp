﻿using System;
using System.Collections.Generic;

namespace WhatsappService.Models
{
    public partial class MSubscription
    {
        public int Id { get; set; }
        public string WhatsAppMobileNumber { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public decimal AmountPaid { get; set; }
        public int EPaperType { get; set; }
        public string SubscriptionType { get; set; }

        public virtual EPaperType EPaperTypeNavigation { get; set; }
    }
}
