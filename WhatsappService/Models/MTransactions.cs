﻿using System;
using System.Collections.Generic;

namespace WhatsappService.Models
{
    public partial class MTransactions
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string WhatsAppMobileNumber { get; set; }
        public decimal Amount { get; set; }
        public int PaperId { get; set; }
    }
}
