﻿using System;
using System.Collections.Generic;

namespace WhatsappService.Models
{
    public partial class MPayments
    {
        public int Id { get; set; }
        public string WhatsAppMobileNumber { get; set; }
        public decimal AmountPaid { get; set; }
        public DateTime DatePaid { get; set; }
        public string PaymentRef { get; set; }
        public bool Successful { get; set; }
    }
}
