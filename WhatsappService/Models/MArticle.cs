﻿using System;
using System.Collections.Generic;

namespace WhatsappService.Models
{
    public partial class MArticle
    {
        public int Id { get; set; }
        public int EArticleType { get; set; }
        public string ArticleTitle { get; set; }
        public string ArticleImage { get; set; }
        public string ArticleBody { get; set; }
        public int PaperId { get; set; }

        public virtual EArticleType EArticleTypeNavigation { get; set; }
        public virtual MPapers Paper { get; set; }
    }
}
