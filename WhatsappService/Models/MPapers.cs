﻿using System;
using System.Collections.Generic;

namespace WhatsappService.Models
{
    public partial class MPapers
    {
        public MPapers()
        {
            MArticle = new HashSet<MArticle>();
        }

        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public int EPaperType { get; set; }
        public DateTime? DatePublished { get; set; }
        public int EPaperStatus { get; set; }
        public bool Pushed { get; set; }

        public virtual EPaperStatus EPaperStatusNavigation { get; set; }
        public virtual EPaperType EPaperTypeNavigation { get; set; }
        public virtual ICollection<MArticle> MArticle { get; set; }
    }
}
