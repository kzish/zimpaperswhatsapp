﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WhatsappService.Models
{
    public partial class dbContext : DbContext
    {
        public dbContext()
        {
        }

        public dbContext(DbContextOptions<dbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<EArticleType> EArticleType { get; set; }
        public virtual DbSet<EPaperStatus> EPaperStatus { get; set; }
        public virtual DbSet<EPaperType> EPaperType { get; set; }
        public virtual DbSet<Logs> Logs { get; set; }
        public virtual DbSet<MArticle> MArticle { get; set; }
        public virtual DbSet<MBreakingNews> MBreakingNews { get; set; }
        public virtual DbSet<MHeadlines> MHeadlines { get; set; }
        public virtual DbSet<MPapers> MPapers { get; set; }
        public virtual DbSet<MPayments> MPayments { get; set; }
        public virtual DbSet<MSession> MSession { get; set; }
        public virtual DbSet<MSettings> MSettings { get; set; }
        public virtual DbSet<MSubscription> MSubscription { get; set; }
        public virtual DbSet<MTransactions> MTransactions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(Globals.DbContextConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.ProviderKey).HasMaxLength(128);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.Name).HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedUserName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<EArticleType>(entity =>
            {
                entity.ToTable("e_article_type");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EPaperStatus>(entity =>
            {
                entity.ToTable("e_paper_status");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EPaperType>(entity =>
            {
                entity.ToTable("e_paper_type");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.PaperType)
                    .IsRequired()
                    .HasColumnName("paper_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PurchasePrice)
                    .HasColumnName("purchase_price")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.SubscriptionDailyPrice)
                    .HasColumnName("subscription_daily_price")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.SubscriptionMonthlyPrice)
                    .HasColumnName("subscription_monthly_price")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.SubscriptionWeeklyPrice)
                    .HasColumnName("subscription_weekly_price")
                    .HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<Logs>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Data1)
                    .HasColumnName("data1")
                    .HasColumnType("text");

                entity.Property(e => e.Data2)
                    .HasColumnName("data2")
                    .HasColumnType("text");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<MArticle>(entity =>
            {
                entity.ToTable("m_article");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ArticleBody)
                    .IsRequired()
                    .HasColumnName("article_body")
                    .IsUnicode(false);

                entity.Property(e => e.ArticleImage)
                    .HasColumnName("article_image")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ArticleTitle)
                    .IsRequired()
                    .HasColumnName("article_title")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EArticleType).HasColumnName("e_article_type");

                entity.Property(e => e.PaperId).HasColumnName("paper_id");

                entity.HasOne(d => d.EArticleTypeNavigation)
                    .WithMany(p => p.MArticle)
                    .HasForeignKey(d => d.EArticleType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_m_article_e_article_type");

                entity.HasOne(d => d.Paper)
                    .WithMany(p => p.MArticle)
                    .HasForeignKey(d => d.PaperId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_m_article_m_papers");
            });

            modelBuilder.Entity<MBreakingNews>(entity =>
            {
                entity.ToTable("m_breaking_news");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DatePosted)
                    .HasColumnName("date_posted")
                    .HasColumnType("datetime");

                entity.Property(e => e.Headline)
                    .IsRequired()
                    .HasColumnName("headline")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ImageUrl)
                    .HasColumnName("image_url")
                    .IsUnicode(false);

                entity.Property(e => e.PaperType).HasColumnName("paper_type");

                entity.Property(e => e.Published).HasColumnName("published");

                entity.Property(e => e.Summary)
                    .IsRequired()
                    .HasColumnName("summary")
                    .IsUnicode(false);

                entity.HasOne(d => d.PaperTypeNavigation)
                    .WithMany(p => p.MBreakingNews)
                    .HasForeignKey(d => d.PaperType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_m_breaking_news_e_paper_type");
            });

            modelBuilder.Entity<MHeadlines>(entity =>
            {
                entity.ToTable("m_headlines");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DatePosted)
                    .HasColumnName("date_posted")
                    .HasColumnType("datetime");

                entity.Property(e => e.Headline)
                    .IsRequired()
                    .HasColumnName("headline")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ImageUrl)
                    .HasColumnName("image_url")
                    .IsUnicode(false);

                entity.Property(e => e.PaperType).HasColumnName("paper_type");

                entity.Property(e => e.Published).HasColumnName("published");

                entity.Property(e => e.Summary)
                    .IsRequired()
                    .HasColumnName("summary")
                    .IsUnicode(false);

                entity.HasOne(d => d.PaperTypeNavigation)
                    .WithMany(p => p.MHeadlines)
                    .HasForeignKey(d => d.PaperType)
                    .HasConstraintName("FK_m_headlines_e_paper_type");
            });

            modelBuilder.Entity<MPapers>(entity =>
            {
                entity.ToTable("m_papers");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("date_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.DatePublished)
                    .HasColumnName("date_published")
                    .HasColumnType("datetime");

                entity.Property(e => e.EPaperStatus).HasColumnName("e_paper_status");

                entity.Property(e => e.EPaperType).HasColumnName("e_paper_type");

                entity.Property(e => e.Pushed).HasColumnName("pushed");

                entity.HasOne(d => d.EPaperStatusNavigation)
                    .WithMany(p => p.MPapers)
                    .HasForeignKey(d => d.EPaperStatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_m_papers_e_paper_status");

                entity.HasOne(d => d.EPaperTypeNavigation)
                    .WithMany(p => p.MPapers)
                    .HasForeignKey(d => d.EPaperType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_m_papers_e_paper_type");
            });

            modelBuilder.Entity<MPayments>(entity =>
            {
                entity.ToTable("m_payments");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AmountPaid)
                    .HasColumnName("amount_paid")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.DatePaid)
                    .HasColumnName("date_paid")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaymentRef)
                    .IsRequired()
                    .HasColumnName("payment_ref")
                    .HasColumnType("text");

                entity.Property(e => e.Successful).HasColumnName("successful");

                entity.Property(e => e.WhatsAppMobileNumber)
                    .IsRequired()
                    .HasColumnName("whats_app_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MSession>(entity =>
            {
                entity.HasKey(e => e.WhatsAppMobileNumber)
                    .HasName("PK__m_sessio__31EF92B163366090");

                entity.ToTable("m_session");

                entity.Property(e => e.WhatsAppMobileNumber)
                    .HasColumnName("whats_app_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Blocked).HasColumnName("blocked");

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IndexCounter).HasColumnName("index_counter");

                entity.Property(e => e.LastMenu)
                    .IsRequired()
                    .HasColumnName("last_menu")
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MSettings>(entity =>
            {
                entity.ToTable("m_settings");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.SubscriptionDailyPrice)
                    .HasColumnName("subscription_daily_price")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.SubscriptionMonthlyPrice)
                    .HasColumnName("subscription_monthly_price")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.SubscriptionWeeklyPrice)
                    .HasColumnName("subscription_weekly_price")
                    .HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<MSubscription>(entity =>
            {
                entity.ToTable("m_subscription");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AmountPaid)
                    .HasColumnName("amount_paid")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.DateEnd)
                    .HasColumnName("date_end")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateStart)
                    .HasColumnName("date_start")
                    .HasColumnType("datetime");

                entity.Property(e => e.EPaperType).HasColumnName("e_paper_type");

                entity.Property(e => e.SubscriptionType)
                    .IsRequired()
                    .HasColumnName("subscription_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WhatsAppMobileNumber)
                    .IsRequired()
                    .HasColumnName("whats_app_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.EPaperTypeNavigation)
                    .WithMany(p => p.MSubscription)
                    .HasForeignKey(d => d.EPaperType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_m_subscription_e_paper_type");
            });

            modelBuilder.Entity<MTransactions>(entity =>
            {
                entity.ToTable("m_transactions");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("money");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaperId).HasColumnName("paper_id");

                entity.Property(e => e.WhatsAppMobileNumber)
                    .IsRequired()
                    .HasColumnName("whats_app_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
        }
    }
}
