﻿using System;
using System.Collections.Generic;

namespace WhatsappService.Models
{
    public partial class EArticleType
    {
        public EArticleType()
        {
            MArticle = new HashSet<MArticle>();
        }

        public int Id { get; set; }
        public string Type { get; set; }

        public virtual ICollection<MArticle> MArticle { get; set; }
    }
}
