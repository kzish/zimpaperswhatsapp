﻿using System;
using System.Collections.Generic;

namespace WhatsappService.Models
{
    public partial class EPaperStatus
    {
        public EPaperStatus()
        {
            MPapers = new HashSet<MPapers>();
        }

        public int Id { get; set; }
        public string Status { get; set; }

        public virtual ICollection<MPapers> MPapers { get; set; }
    }
}
