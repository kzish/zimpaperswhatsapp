﻿using System;
using System.Collections.Generic;

namespace WhatsappService.Models
{
    public partial class MSettings
    {
        public int Id { get; set; }
        public decimal SubscriptionDailyPrice { get; set; }
        public decimal SubscriptionWeeklyPrice { get; set; }
        public decimal SubscriptionMonthlyPrice { get; set; }
    }
}
