﻿using System;
using System.Collections.Generic;

namespace WhatsappService.Models
{
    public partial class Logs
    {
        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public string Data1 { get; set; }
        public string Data2 { get; set; }
    }
}
