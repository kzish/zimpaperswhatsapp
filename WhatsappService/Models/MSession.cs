﻿using System;
using System.Collections.Generic;

namespace WhatsappService.Models
{
    public partial class MSession
    {
        public string WhatsAppMobileNumber { get; set; }
        public string LastMenu { get; set; }
        public DateTime Date { get; set; }
        public int IndexCounter { get; set; }
        public string Data { get; set; }
        public bool Blocked { get; set; }
    }
}
