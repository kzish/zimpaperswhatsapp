﻿using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WalletApi.Controllers;
using Webdev.Payments;
using WhatsappService.Models;

public class Helpers
{
    public static HostingEnvironment host;

    public static string ImageToBase64(string path)
    {
        var base64String = string.Empty;
        using (Image image = Image.FromFile(path))
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, image.RawFormat);
                byte[] imageBytes = ms.ToArray();
                base64String = Convert.ToBase64String(imageBytes);
                ms.Dispose();
            }
            image.Dispose();
        }
        return base64String;
    }


    /// <summary>
    /// return a dynamic object of the session_data.Data
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    public static dynamic ReadSessionData(RecieveMessageCallBack message)
    {
        var db = new dbContext();
        try
        {
            var session_data = db.MSession.Find(message.from);
            dynamic data = JsonConvert.DeserializeObject(session_data.Data);
            return data;
        }
        catch (Exception ex)
        {
            Globals.logger.LogError(ex.Message);
            return null;
        }
        finally
        {
            db.Dispose();
        }
    }

    /// <summary>
    /// inserts or updates a key value pair into the session_data.Data
    /// </summary>
    /// <param name="message"></param>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public static bool WriteSessionData(RecieveMessageCallBack message, string key, string value)
    {
        var db = new dbContext();
        try
        {
            var session_data = db.MSession.Find(message.from);
            dynamic data = null;
            //
            if (!string.IsNullOrEmpty(session_data.Data))
                data = JsonConvert.DeserializeObject(session_data.Data);//get the json data
            else
                data = new JObject();
            //
            data[key] = value;//add the key and value
            session_data.Data = JsonConvert.SerializeObject(data);//convert back to string
            db.Entry(session_data).State = EntityState.Modified;//save the session entry
            db.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            Globals.logger.LogError(ex.Message);
            return false;
        }
        finally
        {
            db.Dispose();
        }
    }

    //Ecocash
    public static bool ExecuteEcocashPayment(string phoneNumber, decimal amount)
    {
       // return true;
        try
        {
            //execute ecocash payment request
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            var string_content_string = $@"{{
                                        ""username"": ""{Globals.ecocash_username}"",
                                        ""password"": ""{Globals.ecocash_password}""
                                    }}";
            var stringContent = new StringContent(string_content_string, Encoding.UTF8, "application/json");

            var json = httpClient
                .PostAsync($"{Globals.smartpay_end_point}/api/users/authenticate", stringContent)
                .Result
                .Content
                .ReadAsStringAsync()
                .Result;
            dynamic json_result = JsonConvert.DeserializeObject(json);

            string auth_token = json_result.token;
            httpClient.DefaultRequestHeaders.Clear();
            httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {auth_token}");
            var reference = phoneNumber.Substring(1) + DateTime.Now.ToString("ddMMyyyyhmmss");

            var response = httpClient
                .GetAsync(
                    $"{Globals.smartpay_end_point}/api/ecocash/payzimpapers?IntergrationId=2&phonenumber={phoneNumber}&reference={reference}&amount={amount}")
                .Result;

            var json_response_string = response
                .Content
                .ReadAsStringAsync()
                .Result;

            dynamic jresponse = JsonConvert.DeserializeObject(json_response_string);
            string StatusCode = jresponse.StatusCode;
            if (StatusCode == "1") return true;
            else return false;
        }
        catch (Exception ex)
        {
            WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            return false;
        }
    }

    public static bool ExecutePaynow( string phoneNumber, decimal amt , string method)
    {
        var reference = phoneNumber.Substring(1) + DateTime.Now.ToString("ddMMyyyyhmmss");
        // Create an instance of the paynow class
        var paynow = new Paynow("4680", "d9beb450-d52a-459f-9c23-8c8e8fdf634a");

        paynow.ResultUrl = "http://example.com/gateways/paynow/update";
        paynow.ReturnUrl = "http://example.com/return?gateway=paynow";
        // The return url can be set at later stages. You might want to do this if you want to pass data to the return url (like the reference of the transaction)

        // Create a new payment 

        var payment = paynow.CreatePayment(reference, "mugonat@gmail.com");

        // Add items to the payment
        payment.Add("Zimpapers", amt);

        var response = paynow.SendMobile(payment, phoneNumber, method);

        // Send payment to paynow
        //  var response = paynow.Send(payment);

        // Check if payment was sent without error
        if (response.Success())
        {
            // Get the url to redirect the user to so they can make payment
            var link = response.RedirectLink();

            // Get the poll url (used to check the status of a transaction). You might want to save this in your DB
            var pollUrl = response.PollUrl();

            // Get the instructions
            var instructions = response.Instructions();

            // Check the status of the transaction with the specified pollUrl
            // Now you see why you need to save that url ;-)

            var loop = true;
            var isPaid = false;
            var count = 0;
            var status = paynow.PollTransaction(pollUrl);
            while (loop && count < 10)
            {
                status = paynow.PollTransaction(pollUrl);
                // var status = paynow.ProcessStatusUpdate(pollUrl);
                if (status.Paid())
                {
                    // Yay! Transaction was paid for
                    isPaid = true;
                    loop = false;
                }
                count++;
                System.Threading.Thread.Sleep(2000);
            }
           // return status;
             return isPaid;
        }
        else
        {
            //return null;
             return false;
        }
    }
    //TODO: Telecash
    public static bool ExecuteTelecashPayment(string phoneNumber, decimal amount)
    {
        return false;
    }

    //TODO: OneMoney
    public static bool ExecuteOneMoneyPayment(string phoneNumber, decimal amount)
    {
        return false;
    }


    public static int IncrementErrorCounter(RecieveMessageCallBack message)
    {
        try
        {
            dynamic session_data_data = ReadSessionData(message);
            int error_count = 0;
            int.TryParse($"{session_data_data.error_count}", out error_count);
            error_count++;
            WriteSessionData(message, "error_count", error_count.ToString());
            return error_count;
        }catch(Exception ex)
        {
            Globals.logger.LogError(ex.Message);
            ResetErrorCounter(message);
            return 0;
        }
    }

    public static void ResetErrorCounter(RecieveMessageCallBack message)
    {
        WriteSessionData(message, "error_count", "0");
    }

    public static int GetErrorCounter(RecieveMessageCallBack message)
    {
        try
        {
            dynamic session_data_data = ReadSessionData(message);
            int error_count = 0;
            int.TryParse($"{session_data_data.error_count}", out error_count);
            return error_count;
        }
        catch (Exception ex)
        {
            Globals.logger.LogError(ex.Message);
            ResetErrorCounter(message);
            return 0;
        }
    }
    
}
