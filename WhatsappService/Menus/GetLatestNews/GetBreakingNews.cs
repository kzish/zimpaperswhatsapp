﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Models;
using WalletApi.Controllers;
using WhatsappService.Models;

namespace Menus
{
    public class GetBreakingNews
    {
        public static void PromptShowLatestBreakingNews(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var sessionData = db.MSession.Find(message.from);

            try
            {
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath +
                                                         "/Menus/GetLatestNews/PromptShowLatestBreakingNews.txt");
                var breakingNewsId = int.Parse(message.content.Split("x")[1]);

                var breakingNews = db.MBreakingNews
                    .FirstOrDefault(i => i.Id == breakingNewsId);

                if (breakingNews != null) //  && message.from == "263773638697" only send to terrence for testing
                {
                    var msg = menu.Replace("{{headline}}", breakingNews.Headline.Trim())
                        .Replace("{{paper_type}}", "Zimpapers Digital News")
                        .Replace("{{date}}", breakingNews.DatePosted.ToString("d-MMM-yyyy"))
                        .Replace("{{summary}}", breakingNews.Summary.Trim());
                    
                    if (string.IsNullOrEmpty(breakingNews.ImageUrl))
                    {
                        WhatsAppWalletApiController.SendMessageText(message.from, msg).Wait();
//                        GupshupWalletApiController.NewSendMessageText(message.from, msg).Wait();
                    }
                    else
                    {
                        WhatsAppWalletApiController.SendMessageImageAndText(message.from,
                            Globals.base_path + "/" + breakingNews.ImageUrl, msg).Wait();
//                        GupshupWalletApiController.NewSendMessageImageAndText(message.from,
//                            Globals.base_path + "/" + breakingNews.ImageUrl, msg).Wait();
                    }
                    sessionData.Date = DateTime.Now;
                    db.SaveChanges();
                }
                else
                    WhatsAppWalletApiController.SendMessageText(message.from, "The requested article is unavailable").Wait();
            }
            catch (Exception exception)
            {
                Globals.logger.LogError(exception.Message);
            }
            finally
            {
                db.Dispose();
            }
        }
        
        public static void ShowAllBreakingNews(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var sessionData = db.MSession.Find(message.from);

            try
            {
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath +
                                                         "/Menus/GetLatestNews/PromptShowLatestBreakingNews.txt");
                var breakingNewsList = db.MBreakingNews
                    .Where(i => i.Published)
                    .Where(i => i.DatePosted >= DateTime.Today)
                    .ToList();

                if (breakingNewsList.Any())    //not empty
                {
                    foreach (var breakingNews in breakingNewsList)
                    {
                        var msg = menu.Replace("{{headline}}", breakingNews.Headline.Trim())
                            .Replace("{{paper_type}}", "Zimpapers Digital News")
                            .Replace("{{date}}", breakingNews.DatePosted.ToString("d-MMM-yyyy"))
                            .Replace("{{summary}}", breakingNews.Summary.Trim());
                    
                        if (string.IsNullOrEmpty(breakingNews.ImageUrl))
                        {
                            WhatsAppWalletApiController.SendMessageText(message.from, msg).Wait();
                        }
                        else
                        {
                            WhatsAppWalletApiController.SendMessageImageAndText(message.from,
                                Globals.base_path + "/" + breakingNews.ImageUrl, msg).Wait();
                        }
                    }
                    sessionData.Date = DateTime.Now;
                    db.SaveChanges();
                }
                else
                    WhatsAppWalletApiController.SendMessageText(message.from, "No breaking news today").Wait();
            }
            catch (Exception exception)
            {
                Globals.logger.LogError(exception.Message);
            }
            finally
            {
                db.Dispose();
            }
        }
    }
}