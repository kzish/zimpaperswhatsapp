﻿using Microsoft.EntityFrameworkCore;
using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WhatsappService.Models;
using Microsoft.Extensions.Logging;

namespace Menus
{
    public class GetLatestNews
    {
        private static int array_limit = Globals.LatestHeadlinesQty;

        public static void PromptShowAllPapers(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            var session_data = db.MSession.Find(message.from);

            try
            {
                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath +
                                                  "/Menus/GetLatestNews/PromptShowAllPapers.txt");
                var paper_types = db.EPaperType.ToList();
                //
                decimal _pages = (decimal) paper_types.Count / (decimal) array_limit;
                int pages = 0;
                if (_pages <= 1) pages = 1;
                else if (paper_types.Count % array_limit == 0) pages = (int) _pages;
                else pages = (int) _pages + 1;

                //
                if (message.content.ToLower() == "n" && ((session_data.IndexCounter + 1) < pages))
                    session_data.IndexCounter++; //go next
                else if (message.content.ToLower() == "n") return; //dont waste a message
                //
                if (message.content.ToLower() == "p" && (session_data.IndexCounter >= 1))
                    session_data.IndexCounter--; //go prev
                else if (message.content.ToLower() == "p") return; //dont waste a message

                //pagination
                var sub_list = paper_types
                    .Skip(array_limit * session_data.IndexCounter)
                    .Take(array_limit)
                    .ToList();

                var list = string.Empty;
                int index = (array_limit * session_data.IndexCounter); //start here


                foreach (var item in sub_list)
                {
                    index++; //dont start at zero
                    list += $"{index} - {item.PaperType.Trim()}{Environment.NewLine}";
                }

                menu = menu.Replace("{{list}}", list);
                menu = menu.Replace("{{pager}}", $"{(session_data.IndexCounter + 1)} of {pages}");
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();


                session_data.Date = DateTime.Now;
                session_data.LastMenu = "GetLatestNews.PromptShowAllPapers";
                db.SaveChanges(); //update session
                db.Dispose();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //goes to GetLatestNews.PromptShowHeadlinesOfSelectedPaperType
        //goe to 
        public static void AcceptSelectedPaperType(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                int selection = int.Parse(message.content.ToLower());
                var paper_types = db.EPaperType.ToList();

                if (selection > 0 && selection <= paper_types.Count)
                {
                    //
                    var selected_paper = paper_types[selection - 1];
                    Helpers.WriteSessionData(message, $"selected_paper_type_id", selected_paper.Id.ToString());
                    //
                    var selected_paper_type = paper_types[selection - 1];


                    //check for valid subscription
                    var valid_subscription_exists = db.MSubscription
                        .Where(i => i.EPaperType == selected_paper_type.Id) //this paper type
                        .Where(i => i.DateEnd >= DateTime.Now.Date) //still valid, not expired
                        .Where(i => i.WhatsAppMobileNumber == message.from) //for me
                        .Any();

                    if (!valid_subscription_exists)
                    {
                        //Payments.PromptShowSubscriptionOptions(message);
                        return;
                    }

                    var session_data = db.MSession.Find(message.from);
                    session_data.Date = DateTime.Now;
                    session_data.LastMenu = "GetLatestNews.AcceptSelectedPaperType";
                    session_data.IndexCounter = 0; //reset before leaving
                    db.SaveChanges(); //update session

                    // GetLatestNews.PromptShowHeadlinesOfSelectedPaperType(message);
                }
                else
                {
                    //dont respond
                }
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        public static void PromptShowHeadlinesOfSelectedPaperType(EPaperType _paper_type = null,
            RecieveMessageCallBack message = null)
        {
            var db = new dbContext();
            var menu = string.Empty;
            var session_data = db.MSession.Find(message.from);

            EPaperType paper_type;
            if (_paper_type != null)
            {
                paper_type = _paper_type;
                Helpers.WriteSessionData(message, "selected_paper_type_id", paper_type.Id.ToString());
            }
            else
            {
                dynamic sess_data = Helpers.ReadSessionData(message);
                int selected_paper_type_id = int.Parse($"{sess_data.selected_paper_type_id}");
                paper_type = db.EPaperType.Find(selected_paper_type_id);
            }

            //do not let user view headlines if they have already viewed all available headlines for that paper
            try
            {
                dynamic sessData = Helpers.ReadSessionData(message);
                var validHeadlineIds = JsonConvert.DeserializeObject<List<int>>($"{sessData.valid_headline_ids}");
                var viewedSummaries = JsonConvert.DeserializeObject<List<int>>($"{sessData.viewed_summaries}");
                if (validHeadlineIds == null)
                {
                    //get a list of ids of headlines of the selected paper
                    validHeadlineIds = new List<int>();
                    var tempHeadlines = db.MHeadlines
                        .Where(i => i.PaperType == paper_type.Id) //for this paper type
                        .Where(i => i.DatePosted > DateTime.Now.AddHours(-24)) //posted within the last 24hrs
                        .OrderByDescending(i => i.Id) 
                        .Take(array_limit) // take only 10 latest
                        .ToList();
                    foreach (var headline in tempHeadlines)
                    {
                        validHeadlineIds.Add(headline.Id);
                    }
                    Helpers.WriteSessionData(message, "valid_headline_ids", JsonConvert.SerializeObject(validHeadlineIds));
                }

                if (viewedSummaries != null && validHeadlineIds.All(viewedSummaries.Contains) && validHeadlineIds.Count == viewedSummaries.Count)
                {
                    //have viewed all the valid headlines
                    WhatsAppWalletApiController.SendMessageText(message.from, "You have viewed all the available news headlines. Try again later to get the latest news updates").Wait();
                    return;
                }
            }
            catch (Exception exception)
            {
                Globals.logger.LogError(exception.Message);
            }
            
            try
            {
                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath +
                                                  "/Menus/GetLatestNews/PromptShowHeadlinesOfSelectedPaperType.txt");
                var headlines = db.MHeadlines
                    .Where(i => i.PaperType == paper_type.Id) //for this paper type
                    .Where(i => i.DatePosted > DateTime.Now.AddHours(-24)) //posted within the last 24hrs
                    .OrderByDescending(i => i.Id) 
                    .Take(array_limit) // take only 10 latest
                    .ToList();
                //
                decimal _pages = (decimal) headlines.Count / (decimal) array_limit;
                int pages = 0;
                if (_pages <= 1) pages = 1;
                else if (headlines.Count % array_limit == 0) pages = (int) _pages;
                else pages = (int) _pages + 1;

                //
//                if (message.content.ToLower() == "n" && ((session_data.IndexCounter + 1) < pages))
//                    session_data.IndexCounter++; //go next
//                else if (message.content.ToLower() == "n") return; //dont waste a message
//                //
//                if (message.content.ToLower() == "p" && (session_data.IndexCounter >= 1))
//                    session_data.IndexCounter--; //go prev
//                else if (message.content.ToLower() == "p") return; //dont waste a message

                //pagination
//                var sub_list = headlines
//                    .Skip(array_limit * session_data.IndexCounter)
//                    .Take(array_limit)
//                    .ToList();

                var list = string.Empty;
                int index = (array_limit * session_data.IndexCounter); //start here
                
                foreach (var item in headlines)
                {
                    index++; //dont start at zero
                    list += $"{index} - {item.Headline.Trim()}{Environment.NewLine}";
                }

                var _paper_types = db.EPaperType.ToList();
                var paper_types = new List<string>();
                foreach (var paper in _paper_types)
                {
                    paper_types.Add($"*{paper.PaperType.Split(" ")[0].ToLower().Trim()}*");
                }

                menu = menu.Replace("{{papers}}", String.Join(" or ", paper_types));

                menu = menu.Replace("{{selected_paper}}", paper_type.PaperType);
                if (string.IsNullOrEmpty(list))
                    menu = menu.Replace("{{list}}", $"No headlines for this paper{Environment.NewLine}");
                else menu = menu.Replace("{{list}}", list);
//                menu = menu.Replace("{{pager}}", $"{(session_data.IndexCounter + 1)} of {pages}");
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                
                session_data.Date = DateTime.Now;
                session_data.LastMenu = "GetLatestNews.PromptShowHeadlinesOfSelectedPaperType";
                db.SaveChanges(); //update session
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                var log = new Logs();
                log.Data1 = ex.Message;
                log.Data2 = ex.StackTrace;
                log.Date = DateTime.Now;
                db.Logs.Add(log);
                db.SaveChanges();
            }
            finally
            {
                db.Dispose();
            }
        }

        public static void PromptShowSummaryOfSelectedHeadline(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            var session_data = db.MSession.Find(message.from);
            dynamic session_data_data = Helpers.ReadSessionData(message);
            try
            {
                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath +
                                                  "/Menus/GetLatestNews/PromptShowSummaryOfSelectedHeadline.txt");
                int selected_paper_type_id = int.Parse($"{session_data_data.selected_paper_type_id}");
                var selected_paper_type = db.EPaperType.Find(selected_paper_type_id);
                int selected_headline_index = int.Parse($"{message.content}");

                var headlines = db.MHeadlines
                    .Where(i => i.PaperType == selected_paper_type_id) //for this paper type
                    .Where(i => i.DatePosted > DateTime.Now.AddHours(-24)) //posted within the last 24hrs
                    .OrderByDescending(i => i.Id) 
                    .Take(array_limit) // take only 10 latest
                    .ToList();

                //sanitize
                if (selected_headline_index <= 0 || selected_headline_index > headlines.Count)
                {
                    //invalid headline
                    WhatsAppWalletApiController.SendMessageText(message.from, "Invalid Option").Wait();
                    return;
                }

                var selected_headline = headlines[selected_headline_index - 1];
                
                //do not let user view the same summary again
                List<int> viewed_summaries = new List<int>();
                try
                {
                    viewed_summaries =
                        JsonConvert.DeserializeObject<List<int>>($"{session_data_data.viewed_summaries}");
                }
                catch (Exception ex)
                {
                    Globals.logger.LogError(ex.Message);
                }

                if (viewed_summaries == null) viewed_summaries = new List<int>();
                if (viewed_summaries.Contains(selected_headline.Id))
                {
                    //already viewed this
                    WhatsAppWalletApiController.SendMessageText(message.from, "You have already viewed this headline").Wait();
                    return;
                }
                else
                {
                    //add this to the list of viewed sumaries
                    viewed_summaries.Add(selected_headline.Id);
                    Helpers.WriteSessionData(message, "viewed_summaries",
                        JsonConvert.SerializeObject(viewed_summaries));
                }

                var _paper_types = db.EPaperType.ToList();
                var paper_types = new List<string>();
                foreach (var paper in _paper_types)
                {
                    paper_types.Add($"*{paper.PaperType.Split(" ")[0].ToLower().Trim()}*");
                }

                menu = menu.Replace("{{selected_paper}}", selected_paper_type.PaperType.Trim());
                menu = menu.Replace("{{headline}}", selected_headline.Headline.Trim());
                menu = menu.Replace("{{summary}}", selected_headline.Summary.Trim());
                menu = menu.Replace("{{papers}}", String.Join(" or ", paper_types));

                if (string.IsNullOrEmpty(selected_headline.ImageUrl))
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                else
                    WhatsAppWalletApiController.SendMessageImageAndText(message.from,
                        Globals.base_path + "/" + selected_headline.ImageUrl, menu).Wait();
                
                session_data.Date = DateTime.Now;
                //dont update the session.LastMenu, let it remain so user can get the next summary immediatly
                //session_data.LastMenu = "GetLatestNews.PromptShowSummaryOfSelectedHeadline";
                //session_data.LastMenu = "GetLatestNews.PromptShowHeadlinesOfSelectedPaperType";
                db.SaveChanges();//update session
                db.Dispose();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                var log = new Logs();
                log.Data1 = ex.Message;
                log.Data2 = ex.StackTrace;
                log.Date = DateTime.Now;
                db.Logs.Add(log);
                db.SaveChanges();
            }
            finally
            {
                db.Dispose();
            }
        }
    }
}