﻿using Microsoft.EntityFrameworkCore;
using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WhatsappService.Models;
using Microsoft.Extensions.Logging;

namespace Menus
{
    public class MySubscriptions
    {

        private static int array_limit = 10;

        public static void ShowMySubScriptions(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            var session_data = db.MSession.Find(message.from);
            dynamic session_data_data = Helpers.ReadSessionData(message);

            try
            {
                //don't let user request subs for a certain number of times within a single session 
                int subs_view_count = 0;
                try
                {
                    subs_view_count =
                        JsonConvert.DeserializeObject<int>($"{session_data_data.subs_view_count}");
                }
                catch (Exception e) 
                {
                    Globals.logger.LogError(e.Message);
                }

                if (subs_view_count >= Globals.MaxSubscriptionsCount)
                {
                    return;
                }
                else
                {
                    //add count 
                    subs_view_count++;
                    Helpers.WriteSessionData(message, "subs_view_count",JsonConvert.SerializeObject(subs_view_count));
                }

                string subsViewCount = (Globals.MaxSubscriptionsCount - subs_view_count).ToString();
                
                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MySubscriptions/ShowMySubScriptions.txt");

                var my_subs_daily = db.MSubscription
                    .Where(i => i.DateEnd >= DateTime.Now.Date)//still valid
                    .Where(i => i.WhatsAppMobileNumber == message.from)//mine
                    .Where(i => i.SubscriptionType.ToLower() == "daily")//daily
                    .Include(i => i.EPaperTypeNavigation)
                    .ToList();

                var my_subs_weekly = db.MSubscription
                    .Where(i => i.DateEnd >= DateTime.Now.Date)//still valid
                    .Where(i => i.WhatsAppMobileNumber == message.from)//mine
                    .Where(i => i.SubscriptionType.ToLower() == "weekly")//weekly
                    .Include(i => i.EPaperTypeNavigation)
                    .ToList();

                var my_subs_monthly = db.MSubscription
                    .Where(i => i.DateEnd >= DateTime.Now.Date)//still valid
                    .Where(i => i.WhatsAppMobileNumber == message.from)//mine
                    .Where(i => i.SubscriptionType.ToLower() == "monthly")//monthly
                    .Include(i => i.EPaperTypeNavigation)
                    .ToList();

                //
                int max_value = (Math.Max(Math.Max(my_subs_daily.Count, my_subs_weekly.Count), my_subs_monthly.Count));
                decimal _pages = (decimal)max_value / (decimal)array_limit;
                int pages = 0;
                if (_pages <= 1) pages = 1;
                else if (max_value % array_limit == 0) pages = (int)_pages;
                else pages = (int)_pages + 1;

                //
                if (message.content.ToLower() == "n" && ((session_data.IndexCounter + 1) < pages)) session_data.IndexCounter++;//go next
                else if (message.content.ToLower() == "n") return;//dont waste a message
                //
                if (message.content.ToLower() == "p" && (session_data.IndexCounter >= 1)) session_data.IndexCounter--;//go prev
                else if (message.content.ToLower() == "p") return;//dont waste a message

                //pagination
                var sub_list1 = my_subs_daily
                    .Skip(array_limit * session_data.IndexCounter)
                    .Take(array_limit)
                    .ToList();

                var sub_list2 = my_subs_weekly
                   .Skip(array_limit * session_data.IndexCounter)
                   .Take(array_limit)
                   .ToList();

                var sub_list3 = my_subs_monthly
                   .Skip(array_limit * session_data.IndexCounter)
                   .Take(array_limit)
                   .ToList();


                var list1 = string.Empty;
                var list2 = string.Empty;
                var list3 = string.Empty;
                int index1 = (array_limit * session_data.IndexCounter);//start here
                int index2 = (array_limit * session_data.IndexCounter);//start here
                int index3 = (array_limit * session_data.IndexCounter);//start here

                foreach (var item in sub_list1)
                {
                    index1++;//dont start at zero
                    list1 += $"{index1} - {item.EPaperTypeNavigation.PaperType}...expires...{item.DateEnd.ToString("yyyy-MM-d")}{Environment.NewLine}";
                }
                foreach (var item in sub_list2)
                {
                    index2++;//dont start at zero
                    list2 += $"{index2} - {item.EPaperTypeNavigation.PaperType}...expires...{item.DateEnd.ToString("yyyy-MM-d")}{Environment.NewLine}";
                }
                foreach (var item in sub_list3)
                {
                    index3++;//dont start at zero
                    list3 += $"{index3} - {item.EPaperTypeNavigation.PaperType}...expires...{item.DateEnd.ToString("yyyy-MM-d")}{Environment.NewLine}";
                }

                if (string.IsNullOrEmpty(list1)) menu = menu.Replace("{{list_daily}}", "No Subscriptions");
                else menu = menu.Replace("{{list_daily}}", list1);

                if (string.IsNullOrEmpty(list2)) menu = menu.Replace("{{list_weekly}}", "No Subscriptions");
                else menu = menu.Replace("{{list_weekly}}", list2);

                if (string.IsNullOrEmpty(list3)) menu = menu.Replace("{{list_monthly}}", "No Subscriptions");
                else menu = menu.Replace("{{list_monthly}}", list3);

                menu = menu.Replace("{{pager}}", $"{(session_data.IndexCounter + 1)} of {pages}");
                menu = menu.Replace("{{subs_view_count}}", subsViewCount);
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();

                session_data.Date = DateTime.Now;
//                session_data.LastMenu = "MySubscriptions.ShowMySubScriptions";
                db.SaveChanges();//update session

            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                var log = new Logs();
                log.Data1 = ex.Message;
                log.Data2 = ex.StackTrace;
                log.Date = DateTime.Now;
                db.Logs.Add(log);
                db.SaveChanges();
            }
            finally
            {
                db.Dispose();
            }
        }

        public static void StopMySubscription(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var sessionData = db.MSession.Find(message.from);
            try
            {
                var mySubscription = db.MSubscription
                    .Where(i => i.DateEnd >= DateTime.Now.Date)//still valid
                    .FirstOrDefault(i => i.WhatsAppMobileNumber == message.from);//mine

                if (mySubscription != null)
                {
                    mySubscription.DateEnd = DateTime.Now.AddDays(-1);  //set end date to yesterday to immediatly stop
                    var msg =
                        $"Your subscription has been terminated. We are sorry so see you go";
                    WhatsAppWalletApiController.SendMessageText(message.from, msg).Wait();
                    sessionData.Date = DateTime.Now;
                    sessionData.LastMenu = "main";
                    db.Logs.Add(new Logs {Data1 = "churn", Data2 = $"{message.from} Unsubscribed", Date = DateTime.Now});
                    db.SaveChanges();
                }
                else
                {
                    WhatsAppWalletApiController.SendMessageText(message.from, "You are not subscribed to this service").Wait();
                }
            }
            catch (Exception e)
            {
                Globals.logger.LogInformation(e.Message);
                WhatsAppWalletApiController.SendErrorMessageText(message.from, e.Message).Wait();
                var log = new Logs();
                log.Data1 = e.Message;
                log.Data2 = e.StackTrace;
                log.Date = DateTime.Now;
                db.Logs.Add(log);
                db.SaveChanges();
            }
            finally
            {
                db.Dispose();
            }
        }
    }

}
