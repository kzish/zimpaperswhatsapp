﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WalletApi.Models;

namespace WalletApi.Menus
{
    public class WalletServices
    {

        private static int array_limit = 5;

        //index menu
        public static string Menu(RecieveMessageCallBack message, string direction)
        {
            try
            {
                List<string> main_menu = new List<string>();
                main_menu.Add("1⃣ - Cash out");
                main_menu.Add("2⃣ - Change pin");
                main_menu.Add("3⃣ - Rubi-Wallet to bank");
                main_menu.Add("4⃣ - Rubi-Wallet to ecocash");
                main_menu.Add("5⃣ - Rubi-Wallet to telecash");
                main_menu.Add("6⃣ - Rubi-Wallet to onemoney");
                main_menu.Add("7⃣ - Bank to Rubi-Wallet");
                main_menu.Add("8⃣ - Ecocash to Rubi-Wallet");
                main_menu.Add("9⃣ - Telecash to Rubi-Wallet");
                main_menu.Add("🔟 - OneMoney to Rubi-Wallet");

                var db = new dbContext();
                var session_data = db.MWalletSession.Find(message.from);

                int pages = main_menu.Count() / array_limit;
                if (direction == "n" && (session_data.IndexCounter < pages)) session_data.IndexCounter++;//go next
                if (direction == "p" && (session_data.IndexCounter >= 1)) session_data.IndexCounter--;//go prev

                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/main_menu.txt");

                //pagination
                var sub_menu = main_menu
                    .Skip(array_limit * session_data.IndexCounter)
                    .Take(array_limit)
                    .ToList();

                menu = menu.Replace("{{list}}", string.Join(Environment.NewLine, sub_menu));
                menu = menu.Replace("{{pager}}", $"{session_data.IndexCounter + 1} of {pages + 1}");
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;


                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.5";
                db.Entry(session_data).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();//update session
                db.Dispose();
                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
        }

    }
}


