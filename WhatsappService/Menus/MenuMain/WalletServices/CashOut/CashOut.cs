﻿using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WalletApi.Models;

namespace WalletApi.Menus
{
    public class CashOut
    {

        private static int array_limit = 5;

        /// <summary>
        /// display request agent code
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string Menu_1(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/CashOut/enter_agent_code.txt");
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;
                var session_data = db.MWalletSession.Find(message.from);
                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.5.1.1";
                db.Entry(session_data).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();//update session
                db.Dispose();
                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }


        /// <summary>
        /// verify agent code and request the amount to cash out
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string Menu_2(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var my_wallet = db.MWallet.Find(message.from);
                var menu = string.Empty;

                var agent = db.MWalletAgents.Where(i => i.AgentCode == message.content).FirstOrDefault();
                //agent does not exist
                if (agent == null)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"This agent {message.content} does not exist.");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/CashOut/enter_agent_code.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }
                //agent is correct store agent code and display confirm
                else
                {
                    Helpers.WriteSessionData(message, "agent_code", message.content);//store agent code
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/CashOut/enter_cash_out_amount.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //
                    var session_data = db.MWalletSession.Find(message.from);
                    session_data.Date = DateTime.Now;
                    session_data.LastMenu = "main.5.1.2";
                    db.SaveChanges();//update session
                    db.Dispose();
                    return "sent";
                }

            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }


        /// <summary>
        /// verify sufficient funds to make the cash out, then proceed to make the cashout
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string Menu_3(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                var my_wallet = db.MWallet.Find(message.from);
                var menu = string.Empty;
                decimal amount_to_cash_out = 0;
                //amount must be a number
                if (!decimal.TryParse(message.content, out amount_to_cash_out))
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Invalid amount entered.{Environment.NewLine}Amount must be a number");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/CashOut/enter_cash_out_amount.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }

                ///* check for sufficient funds in my wallet

                var transaction_fee = Helpers.GetTransactionFee("cash_out", amount_to_cash_out);

                //check balance must be greater than (chash out amount + transaction fee)
                if (my_wallet.Balance < (transaction_fee + amount_to_cash_out))
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Insufficient balance to make this transaction");
                    menu = menu.Replace("*Try again.*", "");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //go to main menu, close this session
                    MenuMain.Menu(message);
                    return "failed";
                }

                //store session data
                Helpers.WriteSessionData(message, "amount_to_cash_out", message.content);//store the amount to cash out
                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/CashOut/confirm_pin_to_cash_out.txt");
                //
                dynamic session_data_data = Helpers.ReadSessionData(message);
                string agent_code = session_data_data.agent_code.ToString();
                string cash_out_amount = session_data_data.amount_to_cash_out.ToString();
                //
                var agent = db.MWalletAgents.Where(i => i.AgentCode == agent_code).FirstOrDefault();
                //
                menu = menu.Replace("{{agent_name}}", agent.AgentName);
                menu = menu.Replace("{{agent_code}}", agent.AgentCode);
                menu = menu.Replace("{{agent_city}}", agent.AgentCity);
                menu = menu.Replace("{{agent_town}}", agent.AgentTown);
                menu = menu.Replace("{{agent_address}}", agent.AgentAddress);
                menu = menu.Replace("{{amount}}", decimal.Parse(cash_out_amount).ToString("0.00"));//amount
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                //
                var session_data = db.MWalletSession.Find(message.from);
                session_data.LastMenu = "main.5.1.3";
                db.SaveChanges();
                return "sent";
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }

        /// <summary>
        /// confirm pin is correct and execute the cash out
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string Menu_4(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                var session_data_data = Helpers.ReadSessionData(message);
                var agent_code = (string)session_data_data.agent_code.ToString();
                var cash_out_amount = (string)session_data_data.amount_to_cash_out.ToString();
                var my_wallet = db.MWallet.Find(message.from);
                var menu = string.Empty;

                //invalid pin
                if (my_wallet.Pin != message.content)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"You have entered an incorrect pin.");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/CashOut/confirm_pin_to_cash_out.txt");
                    var agent = db.MWalletAgents.Where(i => i.AgentCode == agent_code).FirstOrDefault();
                    menu = menu.Replace("{{agent_name}}", agent.AgentName);
                    menu = menu.Replace("{{agent_code}}", agent.AgentCode);
                    menu = menu.Replace("{{agent_city}}", agent.AgentCity);
                    menu = menu.Replace("{{agent_town}}", agent.AgentTown);
                    menu = menu.Replace("{{agent_address}}", agent.AgentAddress);
                    menu = menu.Replace("{{amount}}", decimal.Parse(cash_out_amount).ToString("0.00"));//amount
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }


                ///* execute the cash out
                // deduct the amount from the balance
                // include the cahout charge in the deduction
                // record this trasnaction in the database
                // recorde the fee in the db
                // todo: send confirmation to the agent also


                //cash_out amount
                var deduction_amount = decimal.Parse(cash_out_amount);
                var transaction_fee = Helpers.GetTransactionFee("cash_out", deduction_amount);

                //deduct the amount to cash out
                my_wallet.Balance -= deduction_amount;
                //deduct the fee charged
                my_wallet.Balance -= transaction_fee;


                //record this transaction
                var deduction_transaction = new MTransactions();
                deduction_transaction.WhatsAppMobileNumber = message.from;
                deduction_transaction.Date = DateTime.Now;
                deduction_transaction.Amount = deduction_amount;//dont record the fee here, only record the deduction amount
                deduction_transaction.EnumTransactionType = db.ETransactionTypes.Where(i => i.TransactionType == "cash_out").FirstOrDefault().Id;
                deduction_transaction.From = message.from;//from me to agent
                deduction_transaction.To = agent_code;
                db.MTransactions.Add(deduction_transaction);//save transaction

                //record also the transaction fee into the db
                var mWalletTransactionFees = new MWalletTransactionFees();
                mWalletTransactionFees.Date = DateTime.Now;
                mWalletTransactionFees.WhatsAppMobileNumber = message.from;
                mWalletTransactionFees.ETransactionType = db.ETransactionTypes.Where(i => i.TransactionType == "cash_out").FirstOrDefault().Id;
                mWalletTransactionFees.TransactionFee = transaction_fee;//record the actual fee not the link to the fee, incase that fee ever changes
                db.MWalletTransactionFees.Add(mWalletTransactionFees);
                //
                db.SaveChanges();
                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/CashOut/successfully_cashed_out.txt");
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                MenuMain.Menu(message);//back to main menu
                return "sent";
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }

    }
}


