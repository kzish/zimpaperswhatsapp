﻿using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WhatsappService.Models;

namespace Menus
{
    public class SubscriptionPackages
    {

        private static int array_limit = 5;
        public static void Menu1(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/SubscriptionPackages/menu1.txt");

                var session_data = db.MSession.Find(message.from);
                var paper_types = db.EPaperType.ToList();
                int pages = paper_types.Count / array_limit;

                if (message.content.ToLower() == "n" && (session_data.IndexCounter < pages)) session_data.IndexCounter++;//go next
                if (message.content.ToLower() == "p" && (session_data.IndexCounter >= 1)) session_data.IndexCounter--;//go prev

                //pagination
                var sub_paper_types = paper_types
                    .Skip(array_limit * session_data.IndexCounter)
                    .Take(array_limit)
                    .ToList();

                var list = string.Empty;
                int index = (array_limit * session_data.IndexCounter);//start here


                foreach (var item in sub_paper_types)
                {
                    index++;//dont start at zero
                    list += $"{index} - {item.PaperType}{Environment.NewLine}";
                }

                menu = menu.Replace("{{list}}", list);
                menu = menu.Replace("{{pager}}", $"{session_data.IndexCounter + 1} of {pages + 1}");
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;


                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.2";
                db.SaveChanges();//update session
                db.Dispose();

            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //display the subscription prices
        public static void Menu2(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/SubscriptionPackages/menu2.txt");
                int selection = int.Parse(message.content.ToLower());
                var session_data = db.MSession.Find(message.from);
                var paper_types = db.EPaperType.ToList();

                if (selection > 0 && selection <= paper_types.Count)
                {
                    var selected_paper = paper_types[selection - 1];
                    var subscription_options = string.Empty;
                    subscription_options += $"{selected_paper.PaperType}{Environment.NewLine}{Environment.NewLine}";
                    subscription_options += $"1 - daily {selected_paper.SubscriptionDailyPrice.ToString("0.00")}{Environment.NewLine}";
                    subscription_options += $"2 - weekly {selected_paper.SubscriptionWeeklyPrice.ToString("0.00")}{Environment.NewLine}";
                    subscription_options += $"3 - monthly {selected_paper.SubscriptionMonthlyPrice.ToString("0.00")}{Environment.NewLine}";
                    menu = menu.Replace("{{data}}", subscription_options);
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    session_data.Date = DateTime.Now;
                    session_data.LastMenu = "main.2.subscription_selection";
                    db.SaveChanges();//update session
                    //save the data
                    Helpers.WriteSessionData(message, $"selected_paper_type_id", selected_paper.Id.ToString());
                }
                else
                {
                    //save 
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Selection must be between 1 and {paper_types.Count}");
                    //send error
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //prev menu
                    message.content = "2";
                    session_data.LastMenu = "main";
                    db.SaveChanges();
                    Menu1(message);
                }

            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }


        //select the subscription type daily, monthly, weekly
        //and prompt select payment option
        public static void Menu3(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            var session_data = db.MSession.Find(message.from);
            try
            {
                var selection = int.Parse(message.content);

                //valid selection
                if (selection > 0 && selection <= 3)
                {
                    //check existing subscription
                    //1=daily
                    //2=weekly
                    //3=monthly

                    dynamic session_data_data = Helpers.ReadSessionData(message);
                    int paper_type_id = int.Parse(session_data_data.selected_paper_type_id.ToString());
                    var paper_type = db.EPaperType.Find(paper_type_id);
                    var sub_type = "daily";
                    if (selection == 1) sub_type = "daily";
                    if (selection == 2) sub_type = "weekly";
                    if (selection == 3) sub_type = "monthly";

                    var existing_subscription = db.MSubscription
                        .Where(i => i.WhatsAppMobileNumber == message.from)//from me
                        .Where(i => i.DateEnd >= DateTime.Now)//still valid
                        .Where(i => i.SubscriptionType == sub_type)//the selected sub type
                        .Where(i => i.EPaperType == paper_type_id)//the selected paper type
                        .Any();

                    if (existing_subscription)
                    {
                        //return existing notification
                        menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                        menu = menu.Replace("{{error_message}}", $"You already have an existing subscription");
                        //send error
                        WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                        //prev menu
                        message.content = "2";
                        session_data.Date = DateTime.Now;
                        session_data.LastMenu = "main";
                        db.SaveChanges();
                        Menu1(message);
                    }
                    else //no existing sub prompt enter payment and save selection
                    {
                        Helpers.WriteSessionData(message, "subscription_type", sub_type);
                        menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus//MenuMain/SubscriptionPackages/menu3.txt");
                        //send prompt select payment option
                        WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                        session_data.Date = DateTime.Now;
                        session_data.LastMenu = "main.2.subscription_selection.payment_option_prompt";
                        db.SaveChanges();
                    }
                }
                else//invalid selection
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Selection must be between 1 and 3");
                    //send error
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //prev menu
                    message.content = "2";
                    session_data.Date = DateTime.Now;
                    session_data.LastMenu = "main";
                    db.SaveChanges();
                    Menu1(message);
                }
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }


        //accept the selected payment option for now only ecocash
        //prompt user to enter the ecocash/telecash/onemoney number
        public static void Menu4(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            var session_data = db.MSession.Find(message.from);
            try
            {

                var selection = int.Parse(message.content);
                //valid selection
                if (selection == 1)
                {
                    //check payment selection
                    //1=ecocash
                    Helpers.WriteSessionData(message, "payment_selection_method", "Ecocash");
                    dynamic session_data_data = Helpers.ReadSessionData(message);
                    var paper_type_id = int.Parse($"{session_data_data.selected_paper_type_id}");
                    var paper_type = db.EPaperType.Find(paper_type_id);

                    var sub_type = $"{session_data_data.subscription_type}";
                    decimal sub_amount = 0;
                    if (sub_type.ToLower() == "daily") sub_amount = paper_type.SubscriptionDailyPrice;
                    if (sub_type.ToLower() == "weekly") sub_amount = paper_type.SubscriptionWeeklyPrice;
                    if (sub_type.ToLower() == "monthly") sub_amount = paper_type.SubscriptionMonthlyPrice;


                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/SubscriptionPackages/menu4.txt");
                    menu = menu.Replace("{{paper_type}}", paper_type.PaperType);
                    menu = menu.Replace("{{subscription}}", sub_type);
                    menu = menu.Replace("{{amount}}", sub_amount.ToString("0.00"));
                    menu = menu.Replace("{{payment_type}}", $"{session_data_data.payment_selection_method}");
                    //send 
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    session_data.Date = DateTime.Now;
                    session_data.LastMenu = "main.2.subscription_selection.payment_option_prompt.payment_number";
                    db.SaveChanges();
                }
                else //invalid payment
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Invalid selection");
                    //send error
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                }
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //accept mobile number and execute the payment
        public static void Menu5(RecieveMessageCallBack message)
        {
            dynamic session_data_data = Helpers.ReadSessionData(message);
            var payment_selection_method = $"{session_data_data.payment_selection_method}";
            if (payment_selection_method.ToLower() == "ecocash") EcocashPayment(message);
        }

        
        public static void EcocashPayment(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var session_data = db.MSession.Find(message.from);
                var menu = string.Empty;
                var phone_number = message.content;
                decimal amount = 0;

                dynamic session_data_data = Helpers.ReadSessionData(message);
                var sub_type = $"{session_data_data.subscription_type}";
                var paper_type_id = int.Parse($"{session_data_data.selected_paper_type_id}");
                var paper_type = db.EPaperType.Find(paper_type_id);
                if (sub_type.ToLower() == "daily") amount = paper_type.SubscriptionDailyPrice;
                if (sub_type.ToLower() == "weekly") amount = paper_type.SubscriptionWeeklyPrice;
                if (sub_type.ToLower() == "monthly") amount = paper_type.SubscriptionMonthlyPrice;

                
                if(Helpers.ExecuteEcocashPayment(phone_number,amount))
                {
                    //save subscription
                    var new_subscription = new MSubscription();
                    new_subscription.WhatsAppMobileNumber = message.from;
                    new_subscription.DateStart = DateTime.Now;
                    //1 month subscription mean you will get updates/news for 1 month
                    //1 week subscription mean you will get updates/news for 1 week
                    //1 day subscription mean you will get updates/news for 1 day
                    if (sub_type == "daily")
                    {
                        new_subscription.DateEnd = DateTime.Now.AddDays(1);//subscription is valid for 1 day
                        new_subscription.AmountPaid = paper_type.SubscriptionDailyPrice;
                    }
                    if (sub_type == "weekly")
                    {
                        new_subscription.DateEnd = DateTime.Now.AddDays(7);//subscription is valid for 1 week
                        new_subscription.AmountPaid = paper_type.SubscriptionWeeklyPrice;
                    }
                    if (sub_type == "monthly")
                    {
                        new_subscription.DateEnd = DateTime.Now.AddDays(31);//subscription is valid for 1 month
                        new_subscription.AmountPaid = paper_type.SubscriptionMonthlyPrice;
                    }
                    new_subscription.EPaperType = paper_type_id;
                    new_subscription.SubscriptionType = sub_type;
                    db.MSubscription.Add(new_subscription);
                    db.SaveChanges();
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/SubscriptionPackages/menu5.txt");
                    //send menu
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    MenuMain.Menu1(message);//send back to main menu
                }
                //error
                else
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Ecocash error occurred.");
                    menu = menu.Replace("*Try again.*", "");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    MenuMain.Menu1(message);//send back to main menu
                }
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }




    }

}
