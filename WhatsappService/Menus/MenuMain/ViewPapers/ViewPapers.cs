﻿using Microsoft.EntityFrameworkCore;
using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WhatsappService.Models;

namespace Menus
{
    //show my papers that i have baough or papers within my valid subscriptions
    public class ViewPapers
    {

        //private static int array_limit = 5;

        ////prompt user to select a paper type
        //public static void Menu1(RecieveMessageCallBack message)
        //{
        //    var db = new dbContext();

        //    try
        //    {
        //        var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/ViewPapers/menu1.txt");

        //        var session_data = db.MSession.Find(message.from);
        //        var paper_types = db.EPaperType.ToList();
        //        int pages = paper_types.Count / array_limit;

        //        if (message.content.ToLower() == "n" && (session_data.IndexCounter < pages)) session_data.IndexCounter++;//go next
        //        if (message.content.ToLower() == "p" && (session_data.IndexCounter >= 1)) session_data.IndexCounter--;//go prev

        //        //pagination
        //        var sub_paper_types = paper_types
        //            .Skip(array_limit * session_data.IndexCounter)
        //            .Take(array_limit)
        //            .ToList();

        //        var list = string.Empty;
        //        int index = (array_limit * session_data.IndexCounter);//start here


        //        foreach (var item in sub_paper_types)
        //        {
        //            index++;//dont start at zero
        //            list += $"{index} - {item.PaperType}{Environment.NewLine}";
        //        }

        //        menu = menu.Replace("{{list}}", list);
        //        menu = menu.Replace("{{pager}}", $"{session_data.IndexCounter + 1} of {pages + 1}");
        //        var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;


        //        session_data.Date = DateTime.Now;
        //        session_data.LastMenu = "main.4";
        //        db.SaveChanges();//update session
        //        db.Dispose();

        //    }
        //    catch (Exception ex)
        //    {
        //        WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
        //    }
        //    finally
        //    {
        //        db.Dispose();
        //    }
        //}

        ////accept selected paper type
        ////go to menu3
        //public static void Menu2(RecieveMessageCallBack message)
        //{
        //    var db = new dbContext();
        //    string menu = string.Empty;
        //    try
        //    {
        //        int selection = int.Parse(message.content.ToLower());
        //        var session_data = db.MSession.Find(message.from);
        //        var paper_types = db.EPaperType.ToList();

        //        if (selection > 0 && selection <= paper_types.Count)
        //        {
        //            var selected_paper_type = paper_types[selection - 1];
        //            session_data.IndexCounter = 0;//reet before leaving
        //            db.SaveChanges();
        //            //save selection
        //            Helpers.WriteSessionData(message, $"selected_paper_type_id", selected_paper_type.Id.ToString());
        //            message.content = "p";
        //            Menu3(message);//go to next menu
        //        }
        //        else
        //        {
        //            menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
        //            menu = menu.Replace("{{error_message}}", $"Selection must be between 1 and {paper_types.Count}");
        //            //send error
        //            WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
        //            //prev menu
        //            message.content = "4";
        //            session_data.LastMenu = "main";
        //            db.SaveChanges();
        //            Menu1(message);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
        //    }
        //    finally
        //    {
        //        db.Dispose();
        //    }
        //}


        ////navigate the papers i am entitled to which are in the selected paper type
        ////go to menu 4 if index is selected
        //public static void Menu3(RecieveMessageCallBack message)
        //{
        //    var db = new dbContext();
        //    string menu = string.Empty;
        //    try
        //    {
        //        var session_data = db.MSession.Find(message.from);

        //        dynamic session_data_data = Helpers.ReadSessionData(message);
        //        int selected_paper_type_id = int.Parse($"{session_data_data.selected_paper_type_id}");

        //        //now display my papers for this type
        //        var all_my_subscriptions_for_this_type = db.MSubscription
        //            .Where(i => i.WhatsAppMobileNumber == message.from)//from me
        //            .Where(i => i.DateEnd >= DateTime.Now)//still valid subscription
        //            .Where(i => i.EPaperType == selected_paper_type_id)//for this type
        //            .ToList();

        //        var all_my_payments = db.MPayments
        //            .Where(i => i.WhatsAppMobileNumber == message.from)
        //            .ToList();

        //        var all_papers = db.MPapers
        //            .Where(i => i.DatePublished != null)//all published
        //            .Where(i => i.EPaperType == selected_paper_type_id)//of this type
        //            .OrderByDescending(i => i.DatePublished)//order by date desc
        //            .ToList();

        //        var papers_for_me = new List<MPapers>();//papers i am allowed to read

        //        foreach (var paper in all_papers)
        //        {
        //            //add what i have paid for
        //            foreach (var x in all_my_payments)
        //            {
        //                if (x.PaperId == paper.Id)
        //                {
        //                    if (!papers_for_me.Contains(paper))
        //                    {
        //                        papers_for_me.Add(paper);
        //                    }
        //                }
        //            }
        //            //add what i have subscribed for in this month
        //            foreach (var x in all_my_subscriptions_for_this_type)
        //            {
        //                //daily, weekly, monthly subscription
        //                if (paper.DatePublished >= x.DateStart && paper.DatePublished <= x.DateEnd)
        //                {
        //                    if (!papers_for_me.Contains(paper))
        //                    {
        //                        papers_for_me.Add(paper);
        //                    }
        //                }
        //            }
        //            //add all papers in archive mode. archive mode="free for all people"
        //            if (paper.EPaperStatus == 3)//3==archived
        //            {
        //                if (!papers_for_me.Contains(paper))
        //                {
        //                    papers_for_me.Add(paper);
        //                }
        //            }
        //        }//foreach


        //        int pages = papers_for_me.Count / array_limit;

        //        if (message.content.ToLower() == "n" && (session_data.IndexCounter < pages)) session_data.IndexCounter++;//go next
        //        if (message.content.ToLower() == "p" && (session_data.IndexCounter >= 1)) session_data.IndexCounter--;//go prev

        //        //pagination
        //        var sub_list = papers_for_me
        //            .Skip(array_limit * session_data.IndexCounter)
        //            .Take(array_limit)
        //            .ToList();

        //        var list = string.Empty;
        //        int index = (array_limit * session_data.IndexCounter);//start here
        //        var paper_type = db.EPaperType.Find(selected_paper_type_id);

        //        foreach (var item in sub_list)
        //        {
        //            index++;//dont start at zero
        //            list += $"{index} - {paper_type.PaperType} {item.DatePublished?.ToString("d/MM/yyyy")}{Environment.NewLine}";
        //        }

        //        session_data.Date = DateTime.Now;
        //        session_data.LastMenu = "main.4.paper_type";
        //        db.SaveChanges();

        //        //if index is selected then send to menu4 to show the articles
        //        if (message.content.All(Char.IsDigit) && papers_for_me.Count>0)//if no papers skip
        //        {
        //            var selected_index = int.Parse(message.content);
        //            if (selected_index > 0 && selected_index <= papers_for_me.Count)
        //            {
        //                var selected_paper = papers_for_me[selected_index - 1];
        //                session_data.IndexCounter = 0;//reset before leaving
        //                db.SaveChanges();
        //                //save data and go to next menu
        //                Helpers.WriteSessionData(message, "selected_paper_id", selected_paper.Id.ToString());
        //                message.content = "p";
        //                Menu4(message);//go to menu 4
        //            }
        //            else
        //            {
        //                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
        //                menu = menu.Replace("{{error_message}}", "Invalid selection");
        //                //send error
        //                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
        //            }
        //        }
        //        else //else continue to display the papers in navigation
        //        {
        //            menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/ViewPapers/menu3.txt");
        //            if (papers_for_me.Count == 0)
        //            {
        //                menu = menu.Replace("{{paper_type}}", $"You have no subscriptions for {paper_type.PaperType}");
        //            }
        //            else
        //            {
        //                menu = menu.Replace("{{paper_type}}", paper_type.PaperType + $" ({papers_for_me.Count})");
        //            }
        //            menu = menu.Replace("{{list}}", list);
        //            menu = menu.Replace("{{pager}}", $"{session_data.IndexCounter + 1} of {pages + 1}");
        //            WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
        //    }
        //    finally
        //    {
        //        db.Dispose();
        //    }
        //}


        ////navigate the articles of the selected paper
        ////dispay the article if an index is selected
        //public static void Menu4(RecieveMessageCallBack message)
        //{
        //    var db = new dbContext();
        //    string menu = string.Empty;
        //    try
        //    {
        //        var session_data = db.MSession.Find(message.from);

        //        dynamic session_data_data = Helpers.ReadSessionData(message);
        //        int selected_paper_id = int.Parse($"{session_data_data.selected_paper_id}");


        //        var articles_in_this_paper = db.MArticle
        //            .Where(i => i.PaperId == selected_paper_id)//articles for this paper
        //            .Include(i => i.EArticleTypeNavigation)//include the type
        //            .ToList();

        //        int pages = articles_in_this_paper.Count / array_limit;

        //        if (message.content.ToLower() == "n" && (session_data.IndexCounter < pages)) session_data.IndexCounter++;//go next
        //        if (message.content.ToLower() == "p" && (session_data.IndexCounter >= 1)) session_data.IndexCounter--;//go prev

        //        //pagination
        //        var sub_list = articles_in_this_paper
        //            .Skip(array_limit * session_data.IndexCounter)
        //            .Take(array_limit)
        //            .ToList();

        //        var list = string.Empty;
        //        int index = (array_limit * session_data.IndexCounter);//start here

        //        foreach (var item in sub_list)
        //        {
        //            index++;//dont start at zero
        //            var title = item.ArticleTitle;
        //            if (title.Length > 11) title = title.Substring(0, 11)+"...";
        //            list += $"{index} - {item.EArticleTypeNavigation.Type} {title}{Environment.NewLine}";
        //        }

        //        session_data.Date = DateTime.Now;
        //        session_data.LastMenu = "main.4.paper_type.paper_selected";
        //        db.SaveChanges();

        //        //if index is selected then display the selected article and return to the same menu to show the list of articles
        //        if (message.content.All(Char.IsDigit) && articles_in_this_paper.Count > 0)//if no papers skip
        //        {
        //            var selected_index = int.Parse(message.content);
        //            if (selected_index > 0 && selected_index <= articles_in_this_paper.Count)
        //            {
        //                var selected_article = articles_in_this_paper[selected_index - 1];
        //                //display article and return the same menu
        //                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/ViewPapers/article.txt");
        //                menu = menu.Replace("{{title}}", selected_article.ArticleTitle);
        //                menu = menu.Replace("{{text}}", selected_article.ArticleBody);
        //                if(!string.IsNullOrEmpty(selected_article.ArticleImage))//has image
        //                {
        //                    var image = $"{Globals.base_path}/uploads/{selected_article.ArticleImage}";
        //                    WhatsAppWalletApiController.SendMessageImageAndText(message.from,image,menu).Wait();
        //                    Thread.Sleep(3000);//wait till image is sent
        //                    //re-send this menu after the article
        //                    message.content = "0";//not a valid index
        //                    Menu4(message);//resend this same menu
        //                }
        //                else//has no image
        //                {
        //                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
        //                    //re-send this menu after the article
        //                    message.content = "0";//not a valid index
        //                    Menu4(message);//resend this same menu
        //                }
                        
        //            }
        //            else
        //            {
        //                //resending the same menu4
        //                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/ViewPapers/menu4.txt");
        //                menu = menu.Replace("{{list}}", list);
        //                menu = menu.Replace("{{pager}}", $"{session_data.IndexCounter + 1} of {pages + 1}");
        //                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
        //            }
        //        }
        //        else //else continue to display the articles in navigation
        //        {
        //            menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/ViewPapers/menu4.txt");
        //            menu = menu.Replace("{{list}}", list);
        //            menu = menu.Replace("{{pager}}", $"{session_data.IndexCounter + 1} of {pages + 1}");
        //            WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
        //    }
        //    finally
        //    {
        //        db.Dispose();
        //    }
        //}





    }

}
