﻿using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WalletApi.Models;

namespace WalletApi.Menus
{
    public class TransferFunds
    {
        //index
        public static string Menu(RecieveMessageCallBack message)
        {
            try
            {
                var db = new dbContext();
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/TransferFunds/main.4.1.txt");
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;
                var session_data = db.MWalletSession.Find(message.from);
                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.4";
                db.Entry(session_data).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();//update session
                db.Dispose();
                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
        }

        /// <summary>
        /// accept the number (rubi-wallet) to transfer funds to
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string Menu_1(RecieveMessageCallBack message)
        {
            try
            {

                var db = new dbContext();

                var rubi_wallet_to_recieve_transfer_exist = db.MWallet.Where(i => i.WhatsAppMobileNumber == message.content).Any();

                var menu = string.Empty;

                //check if the wallet we want to transfer to exists
                if (!rubi_wallet_to_recieve_transfer_exist)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"This rubi-wallet *{message.content}*{Environment.NewLine}does not exist");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/TransferFunds/main.4.1.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }

                //cannot send to yourself
                if (message.content == message.from)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"You cannot transfer to yourself");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/TransferFunds/main.4.1.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }


                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/TransferFunds/main.4.2.txt");
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;
                //
                Helpers.WriteSessionData(message, "receiver_account", message.content);//store the number to transfer funds to
                //
                var session_data = db.MWalletSession.Find(message.from);
                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.4.1";
                db.SaveChanges();//update session
                db.Dispose();
                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
        }

        /// <summary>
        /// accept the amount to transfer and validate that there are sufficient funds
        /// display confirm transfer text
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string Menu_2(RecieveMessageCallBack message)
        {
            try
            {
                var menu = string.Empty;

                var db = new dbContext();

                decimal amount_to_transfer = 0;
                //check valid amount
                try
                {
                    amount_to_transfer = decimal.Parse(message.content);
                }
                catch (Exception ex)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Invalid amount *{message.content}*{Environment.NewLine}Enter a number");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/TransferFunds/main.4.2.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }

                var my_wallet = db.MWallet.Find(message.from);

                //check amount entered is not negative or zero
                if (amount_to_transfer <= 0)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Amount must be greater than 0");
                    menu = menu.Replace("*Try again.*", "");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/TransferFunds/main.4.2.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }

                ///* check sufficient credit, 

                var transaction_fee = Helpers.GetTransactionFee("wallet_to_wallet_transfer", amount_to_transfer);

                //balance must be greater than (amount to transfer + transaction fee)
                if (my_wallet.Balance < (amount_to_transfer + transaction_fee))
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Insufficient balance to make this transaction");
                    menu = menu.Replace("*Try again.*", "");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    MenuMain.Menu(message);//Menu main
                    return "failed";
                }

                //
                Helpers.WriteSessionData(message, "amount_to_transfer", amount_to_transfer.ToString("0.00"));//store the number to transfer funds to as well as the amount to transfer
                //
                var session_data_data = Helpers.ReadSessionData(message);
                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/TransferFunds/main.4.3.txt");
                menu = menu.Replace("{{amount}}", session_data_data.amount_to_transfer.ToString());
                menu = menu.Replace("{{wallet}}", session_data_data.receiver_account.ToString());
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;
                //
                var session_data = db.MWalletSession.Find(message.from);
                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.4.2";
                db.SaveChanges();//update session
                db.Dispose();
                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
        }


        /// <summary>
        /// prompt confirm details
        /// recieve confirm the transfer input, and display request pin
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string Menu_3(RecieveMessageCallBack message)
        {
            try
            {
                var menu = string.Empty;
                var db = new dbContext();
                var session_data = db.MWalletSession.Find(message.from);


                //confirmed
                if (message.content == "1")
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/TransferFunds/main.4.4.txt");
                    var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;
                    session_data.Date = DateTime.Now;
                    session_data.LastMenu = "main.4.3";
                    db.Entry(session_data).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();//update session
                    db.Dispose();
                    return response.Value.ToString();
                }
                //cancelled
                else if (message.content == "2")
                {
                    MenuMain.Menu(message);//go to Menu main
                    return "canceled";
                }

                //invalid input
                else
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"You have not confirmed this transaction");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    //send prev menu
                    var session_data_data = Helpers.ReadSessionData(message);
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/TransferFunds/main.4.3.txt");
                    menu = menu.Replace("{{amount}}", session_data_data.amount_to_transfer.ToString());
                    menu = menu.Replace("{{wallet}}", session_data_data.receiver_account.ToString());
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    return "failed";
                }

            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
        }



        /// <summary>
        /// recieve the pin, if correct execute the transfer
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string Menu_4(RecieveMessageCallBack message)
        {
            try
            {
                var db = new dbContext();

                var menu = string.Empty;
                var my_wallet = db.MWallet.Find(message.from);
               // var session_data = db.MWalletSession.Find(message.from);


                //wrong pin
                //todo: encrypt pin
                if (message.content != my_wallet.Pin)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Incorrect pin");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/TransferFunds/main.4.4.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    db.Dispose();
                    return "failed";
                }
                else//correct pin
                {

                    ///* do the transfer
                    //add funds to recipient account
                    var session_data_data = Helpers.ReadSessionData(message);
                    var transfer_amount = decimal.Parse(session_data_data.amount_to_transfer.ToString());
                    var transaction_fee = Helpers.GetTransactionFee("wallet_to_wallet_transfer", transfer_amount);


                    //receiver wallet
                    var reciever_wallet = db.MWallet.Find(session_data_data.receiver_account.ToString());
                    reciever_wallet.Balance += transfer_amount;//add amount to reciepient wallet
                    my_wallet.Balance -= transfer_amount;//deduct amount from sender wallet
                    my_wallet.Balance -= transaction_fee;//deduct transaction fee from sender wallet

                    //store the transactions one for sender the other for reciever
                    //sender transaction record
                    var transaction_1 = new MTransactions();
                    transaction_1.WhatsAppMobileNumber = message.from;
                    transaction_1.Date = DateTime.Now;
                    transaction_1.Amount = transfer_amount;
                    transaction_1.EnumTransactionType = 1;
                    transaction_1.From = message.from;
                    transaction_1.To = session_data_data.receiver_account.ToString();
                    //reciever transaction record
                    var transaction_2 = new MTransactions();
                    transaction_2.WhatsAppMobileNumber = transaction_1.To;
                    transaction_2.Date = DateTime.Now;
                    transaction_2.Amount = transfer_amount;
                    transaction_2.EnumTransactionType = 1;
                    transaction_2.From = message.from;
                    transaction_2.To = session_data_data.receiver_account.ToString();

                    //record also the transaction fee into the db
                    var mWalletTransactionFees = new MWalletTransactionFees();
                    mWalletTransactionFees.Date = DateTime.Now;
                    mWalletTransactionFees.WhatsAppMobileNumber = message.from;
                    mWalletTransactionFees.ETransactionType = db.ETransactionTypes.Where(i => i.TransactionType == "wallet_to_wallet_transfer").FirstOrDefault().Id;
                    mWalletTransactionFees.TransactionFee = transaction_fee;//record the actual fee not the link to the fee
                    //
                    db.MWalletTransactionFees.Add(mWalletTransactionFees);
                    db.MTransactions.Add(transaction_1);
                    db.MTransactions.Add(transaction_2);
                    //
                    db.SaveChanges();

                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/TransferFunds/main.4.5.txt");
                    var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;
                    MenuMain.Menu(message);//go back to main menu this transfer is complete
                    db.Dispose();
                    return response.Value.ToString();
                }
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }

        }





    }
}
