﻿using Microsoft.EntityFrameworkCore;
using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WhatsappService.Models;

namespace Menus
{
    public class Payments
    {
        private static int array_limit = 10;

        public static void PromptShowSubscriptionOptions(EPaperType paper_type, RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;

            try
            {
                //save the selected paper type id
                Helpers.WriteSessionData(message, "selected_paper_type_id", paper_type.Id.ToString());
                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath +
                                                  "/Menus/Payments/PromptShowSubscriptionOptions.txt");

                var session_data = db.MSession.Find(message.from);

                var paper_types = db.EPaperType.ToList();
                var paper_types_types = new List<string>();
                var settings = db.MSettings.First();
                foreach (var pt in paper_types)
                {
                    paper_types_types.Add($"*{pt.PaperType}*");
                }

                menu = menu.Replace("{{selected_paper}}", paper_type.PaperType);
                menu = menu.Replace("{{daily}}", settings.SubscriptionDailyPrice.ToString("0.00"));
                menu = menu.Replace("{{weekly}}", settings.SubscriptionWeeklyPrice.ToString("0.00"));
                menu = menu.Replace("{{monthly}}", settings.SubscriptionMonthlyPrice.ToString("0.00"));
                menu = menu.Replace("{{papers_available}}", string.Join(" ,", paper_types_types));

                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();

                session_data.Date = DateTime.Now;
                session_data.LastMenu = "Payments.PromptShowSubscriptionOptions";
                db.SaveChanges(); //update session
                db.Dispose();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //initiates prompt payment
        public static void AcceptSubscriptionType(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                //int input_split_length = message.content.Split("#").Length;

                ////validate and sanitize data

                ////input length must be 2
                //if (input_split_length != 2)
                //{
                //    WhatsAppWalletApiController.SendMessageText(message.from, "Invalid Option").Wait();
                //    return;
                //}

                var subscription_type_selection = "2";
                //var subscription_type_selection = message.content.Split("#")[0];

                //subscription_type_selection must be 1,2,3
                List<string> valid_subscription_types = new List<string>() {"1", "2", "3"};
                if (!valid_subscription_types.Contains(subscription_type_selection))
                {
                    WhatsAppWalletApiController.SendMessageText(message.from, "Invalid Option").Wait();
                    return;
                }

                var subscription_mobile_money_number = message.content;
                //var subscription_mobile_money_number = message.content.Split("#")[1];

                var paper_types = db.EPaperType.ToList();
                var paper_types_types = new List<string>();
                foreach (var pt in paper_types)
                {
                    paper_types_types.Add(pt.PaperType.ToLower());
                }

                //selected paper type
                //if (!paper_types_types.Contains(subscription_paper_selection.ToLower()))
                //{
                //    return;
                //}

                List<string> ecocash_prefixes = new List<string>() {"077", "078"};
                List<string> telecash_prefixes = new List<string>() {"073"};
                List<string> onemoney_prefixes = new List<string>() {"071"};
                //List<string> zimpapers_staff_prefixes = new List<string>() {"500"};

                var mobile_money_number_prefix = subscription_mobile_money_number.Substring(0, 3);

                bool payment_successful = false;
                var settings = db.MSettings.First();
                decimal amount_to_pay = 0;
                if (subscription_type_selection == "1") amount_to_pay = settings.SubscriptionDailyPrice; //daily
                else if (subscription_type_selection == "2") amount_to_pay = settings.SubscriptionWeeklyPrice; //weekly
                else if (subscription_type_selection == "3")
                    amount_to_pay = settings.SubscriptionMonthlyPrice; //monthly

                if (ecocash_prefixes.Contains(mobile_money_number_prefix))
                {
                    //Smartpay
                    payment_successful = Helpers.ExecuteEcocashPayment(subscription_mobile_money_number, amount_to_pay);

                    //Paynow
                    //payment_successful = Helpers.ExecutePaynow(subscription_mobile_money_number, amount_to_pay, "ecocash");
                }
                //else if (zimpapers_staff_prefixes.Contains(mobile_money_number_prefix))
                else if (message.from == "263717770666" || message.from == "263773919335")
                {
                    payment_successful = true;    // zimpapers staff just subscribe
                }
                //else if (telecash_prefixes.Contains(mobile_money_number_prefix))
                //{
                //    payment_successful = Helpers.ExecuteTelecashPayment(subscription_mobile_money_number, amount_to_pay);
                //}
                //else if (onemoney_prefixes.Contains(mobile_money_number_prefix))
                //{
                //    payment_successful = Helpers.ExecutePaynow(subscription_mobile_money_number, amount_to_pay, "onemoney");
                //}
                else
                {
                    WhatsAppWalletApiController.SendMessageText(message.from, "Invalid mobile money number").Wait();
                    return; //invalid mobile money number
                }

                if (payment_successful) //save the subscription and redirect back to the headlines of that subscription
                {
                    dynamic session_data_data = Helpers.ReadSessionData(message);
                    int selected_paper_type_id = int.Parse($"{session_data_data.selected_paper_type_id}");
                    var selected_paper_type = db.EPaperType.Find(selected_paper_type_id);
                    //
                    var subscription = new MSubscription();
                    subscription.WhatsAppMobileNumber = message.from;
                    subscription.DateStart = DateTime.Now;
                    //
                    if (subscription_type_selection == "1") subscription.DateEnd = DateTime.Now.AddDays(1); //daily
                    else if (subscription_type_selection == "2")
                        subscription.DateEnd = DateTime.Now.AddDays(7); //weekly
                    else if (subscription_type_selection == "3")
                        subscription.DateEnd = DateTime.Now.AddDays(31); //monthly
                    //
                    subscription.AmountPaid = amount_to_pay;
                    subscription.EPaperType = selected_paper_type_id;
                    //
                    if (subscription_type_selection == "1") subscription.SubscriptionType = "Daily";
                    else if (subscription_type_selection == "2") subscription.SubscriptionType = "Weekly";
                    else if (subscription_type_selection == "3") subscription.SubscriptionType = "Monthly";
                    //
                    db.MSubscription.Add(subscription);
                    db.SaveChanges();

                    //redirect to the headlines of this paper
                    WhatsAppWalletApiController.SendMessageText(message.from,
                            $"✅✅✅Successfully subscribed to *{selected_paper_type.PaperType}* {subscription.SubscriptionType}." +
                            $"\nTo terminate your subscription before it expires, send *stop*")
                        .Wait();
                    GetLatestNews.PromptShowHeadlinesOfSelectedPaperType(selected_paper_type, message);
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Payment Failed");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    Helpers.IncrementErrorCounter(message);
                }
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                var log = new Logs();
                log.Data1 = ex.Message;
                log.Data2 = ex.StackTrace;
                log.Date = DateTime.Now;
                db.Logs.Add(log);
                db.SaveChanges();
            }
            finally
            {
                db.Dispose();
            }
        }

        public static void AutoFreeSubscription(EPaperType paperType, RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                //only automatically subscribe weekly
                var subscription = new MSubscription
                {
                    WhatsAppMobileNumber = message.from,
                    DateStart = DateTime.Now,
                    DateEnd = DateTime.Now.AddDays(7),
                    AmountPaid = 0,
                    EPaperType = paperType.Id,
                    SubscriptionType = "Weekly"
                };
                db.MSubscription.Add(subscription);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                var log = new Logs {Data1 = ex.Message, Data2 = ex.StackTrace, Date = DateTime.Now};
                db.Logs.Add(log);
                db.SaveChanges();
            }
            finally
            {
                db.Dispose();
            }
        }

        public static void PromptShowHeadlinesOfSelectedPaperType(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            var session_data = db.MSession.Find(message.from);
            dynamic session_data_data = Helpers.ReadSessionData(message);
            try
            {
                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath +
                                                  "/Menus/GetLatestNews/PromptShowHeadlinesOfSelectedPaperType.txt");
                int selected_paper_type_id = int.Parse($"{session_data_data.selected_paper_type_id}");
                var selected_paper_type = db.EPaperType.Find(selected_paper_type_id);
                var headlines = db.MHeadlines
                    .Where(i => i.PaperType == selected_paper_type_id)
                    .ToList();
                //
                decimal _pages = (decimal) headlines.Count / (decimal) array_limit;
                int pages = 0;
                if (_pages <= 1) pages = 1;
                else if (headlines.Count % array_limit == 0) pages = (int) _pages;
                else pages = (int) _pages + 1;

                //
                if (message.content.ToLower() == "n" && ((session_data.IndexCounter + 1) < pages))
                    session_data.IndexCounter++; //go next
                else if (message.content.ToLower() == "n") return; //dont waste a message
                //
                if (message.content.ToLower() == "p" && (session_data.IndexCounter >= 1))
                    session_data.IndexCounter--; //go prev
                else if (message.content.ToLower() == "p") return; //dont waste a message

                //pagination
                var sub_list = headlines
                    .Skip(array_limit * session_data.IndexCounter)
                    .Take(array_limit)
                    .ToList();

                var list = string.Empty;
                int index = (array_limit * session_data.IndexCounter); //start here


                foreach (var item in sub_list)
                {
                    index++; //dont start at zero
                    list += $"{index} - {item.Headline}{Environment.NewLine}";
                }

                menu = menu.Replace("{{selected_paper}}", selected_paper_type.PaperType);
                if (string.IsNullOrEmpty(list))
                    menu = menu.Replace("{{list}}", $"No headlines for this paper{Environment.NewLine}");
                else menu = menu.Replace("{{list}}", list);
                menu = menu.Replace("{{pager}}", $"{(session_data.IndexCounter + 1)} of {pages}");
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();


                session_data.Date = DateTime.Now;
                session_data.LastMenu = "GetLatestNews.PromptShowHeadlinesOfSelectedPaperType";
                db.SaveChanges(); //update session
                db.Dispose();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }
    }
}