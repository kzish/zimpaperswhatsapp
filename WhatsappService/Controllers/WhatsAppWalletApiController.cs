﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Menus;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WhatsappService.Models;

namespace WalletApi.Controllers
{
    /// <summary>
    /// this is a proxy class that will send messages on the selected platforms
    /// </summary>
    public class WhatsAppWalletApiController : Controller
    {
        private static int platform = Globals.Platform;
        private static List<string> start_text = new List<string> {"hi", "news", "#"}; //start texts
        
        //fetch the session data
        public static MSession GetSessionData(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                var session_data = db.MSession.FirstOrDefault(i => i.WhatsAppMobileNumber == message.from);

                if (session_data == null)
                {
                    //if no session exists create new session and save it
                    session_data = new MSession();
                    session_data.WhatsAppMobileNumber = message.from;
                    session_data.Date = DateTime.Now;
                    session_data.LastMenu = "main";
                    session_data.IndexCounter = 0;
                    db.MSession.Add(session_data);
                    db.SaveChanges();
                }
                else
                {
                    //clear sessions when validity period expires
                    if (session_data.Date < DateTime.Now.AddHours(-Globals.SessionValidityPeriod))
                        ClearMySession(session_data);
                }

                return session_data;
            }
            catch (Exception ex)
            {
                Globals.logger.LogError(ex.Message);
                return null;
            }
            finally
            {
                db.Dispose();
            }
        }

        //clear my session to start afresh
        private static void ClearMySession(MSession oldSession)
        {
            var db = new dbContext();
            var oldWhatsAppMobileNumber = oldSession.WhatsAppMobileNumber;

            try
            {
                //remove old session to clear session
                db.MSession.Remove(oldSession);

                //add new blank session
                var new_session = new MSession
                {
                    WhatsAppMobileNumber = oldWhatsAppMobileNumber, Date = DateTime.Now, LastMenu = "main",
                    IndexCounter = 0
                };
                db.MSession.Add(new_session);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Globals.logger.LogError(ex.Message);
            }
            finally
            {
                db.Dispose();
            }
        }

        //receive message callback and executes the menu
        public static void RecieveMessageCallBack(RecieveMessageCallBack message)
        {
            //get session
            var sessionData = GetSessionData(message);

            //list papers
            List<EPaperType> paperTypes;
            
            using (var db = new dbContext())
            {
                paperTypes = db.EPaperType.ToList();
                db.Dispose();
            }

            //selected paper
            //user typed 'news'
            var selectedPaper = paperTypes.FirstOrDefault(i => i.PaperType.ToLower().Contains("news"));
            if (message.content.ToLower() == "news" && selectedPaper != null)
            {
                //check for valid subscription
                if (!IsSubscriptionValid(message, selectedPaper)) 
                    return;

                //show headlines for this paper
                GetLatestNews.PromptShowHeadlinesOfSelectedPaperType(selectedPaper, message);
            }
            
            else if (sessionData.LastMenu == "GetLatestNews.PromptShowHeadlinesOfSelectedPaperType" &&
                     (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                GetLatestNews.PromptShowHeadlinesOfSelectedPaperType(selectedPaper, message); //navigate headlines

            else if (sessionData.LastMenu == "GetLatestNews.PromptShowHeadlinesOfSelectedPaperType" &&
                     message.content.All(Char.IsDigit))
                GetLatestNews.PromptShowSummaryOfSelectedHeadline(message); //show the summary

            //payments
            else if (sessionData.LastMenu == "Payments.PromptShowSubscriptionOptions" &&
                     message.content.ToLower() != "subs")
                Payments.AcceptSubscriptionType(message); //Accept the payment information

            //my subscriptions
            else if (message.content.ToLower() == "subs")
                MySubscriptions.ShowMySubScriptions(message);
            
            //stop subscription
            else if (message.content.ToLower() == "stop")
                MySubscriptions.StopMySubscription(message);
            
            //full breaking news story
            else if (message.content.ToLower().Contains("breaking")){
                //check for valid subscription
                if (!IsSubscriptionValid(message, selectedPaper)) 
                    return;
                GetBreakingNews.PromptShowLatestBreakingNews(message);
            }
            
            //all breaking news stories
            else if (message.content.ToLower() == "yes"){
                //check for valid subscription
                if (!IsSubscriptionValid(message, selectedPaper)) 
                    return;
                GetBreakingNews.ShowAllBreakingNews(message);
            }

            else if (sessionData.LastMenu == "MySubscriptions.ShowMySubScriptions" &&
                     (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                MySubscriptions.ShowMySubScriptions(message);
            else
                SendMessageText(message.from, "Welcome to *Zimpapers Digital News.* Send the word *news* to start").Wait();

            try
            {
                //    int error_counter = Helpers.GetErrorCounter(message);
                //    if (error_counter == 3)
                //    {
                //        //maximum trials exceeded, send message
                //        WhatsAppWalletApiController.SendMessageText(message.from, $"❌❌❌Exceeded Maximum Trials{Environment.NewLine}Please send WhatsApp to xxx,xxx,xxxx.").Wait();
                //        Helpers.IncrementErrorCounter(message);
                //        using (var db = new dbContext())
                //        {
                //            var sess = db.MSession.Find(message.from);
                //            sess.Blocked = true;
                //            db.SaveChanges();
                //            db.Dispose();
                //        }
                //        return;
                //    }
                //    else if (error_counter > 3)
                //    {
                //        using (var db = new dbContext())
                //        {
                //            var sess = db.MSession.Find(message.from);
                //            sess.Blocked = true;
                //            db.SaveChanges();
                //            db.Dispose();
                //        }
                //        return;
                //    }

                //    //main menu
                //    if (start_text.Contains(message.content.ToLower()))
                //        MenuMain.Menu1(message);//Menu main

                // main > option 1 MySubscriptions

                //else if (session_data.LastMenu == "main" && message.content.ToLower() == "1")
                //    MySubscriptions.ShowMySubScriptions(message);//show and navigate my subscriptions

                //else if (session_data.LastMenu == "MySubscriptions.ShowMySubScriptions" && (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                //    MySubscriptions.ShowMySubScriptions(message);//show and navigate my subscriptions

                // main > option 2 GetLatestNews

                //else if (session_data.LastMenu == "main" && message.content.ToLower() == "2")
                //    GetLatestNews.PromptShowAllPapers(message);//show all papers

                //else if (session_data.LastMenu == "GetLatestNews.PromptShowAllPapers" && (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                //    GetLatestNews.PromptShowAllPapers(message);//navigate the paper type

                //else if (session_data.LastMenu == "GetLatestNews.PromptShowAllPapers" && message.content.All(char.IsDigit))
                //    GetLatestNews.AcceptSelectedPaperType(message);//accept the paper type

                //else if (session_data.LastMenu == "GetLatestNews.PromptShowHeadlinesOfSelectedPaperType" && (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                //    GetLatestNews.PromptShowHeadlinesOfSelectedPaperType(message);//navigate the headlines for the selected paper type

                //else if (session_data.LastMenu == "GetLatestNews.PromptShowHeadlinesOfSelectedPaperType" && message.content.All(char.IsDigit))
                //    GetLatestNews.PromptShowSummaryOfSelectedHeadline(message);//display the summary of the selected headline

                //payments
                //else if (session_data.LastMenu == "Payments.PromptShowSubscriptionOptions")
                //    Payments.AcceptSubscriptionType(message);//Accept the payment information

                //else
                //{

                //    WhatsAppWalletApiController
                //        .SendMessageText(message.from, $"Invalid response, try again or enter '{String.Join(" or ", start_text)}' to return to main menu").Wait();
                //    //error_counter = Helpers.IncrementErrorCounter(message);
                //    //if (error_counter >= 3)
                //    //{
                //    //    WhatsAppWalletApiController
                //    //                           .SendMessageText(message.from, $"❌❌❌Exceeded Maximum Trials{Environment.NewLine}Please sent WhatsApp to xxx,xxx,xxxx.").Wait();
                //    //}
                //}
            }
            catch (Exception ex)
            {
                SendErrorMessageText(message.from, ex.Message).Wait();
            }
        }

        private static bool IsSubscriptionValid(RecieveMessageCallBack message, EPaperType selectedPaper)
        {
            bool validSubscriptionExists;
            using (var db = new dbContext())
            {
                validSubscriptionExists = db.MSubscription
                    .Where(i => i.EPaperType == selectedPaper.Id) //this paper type
                    .Where(i => i.DateEnd >= DateTime.Now.Date) //still valid, not 
                    .Any(i => i.WhatsAppMobileNumber == message.from); //for me
                db.Dispose();
            }

            if (validSubscriptionExists) 
                return true;
            Payments.PromptShowSubscriptionOptions(selectedPaper, message);
            //Payments.AutoFreeSubscription(selectedPaper, message);
            return false;
        }

        //send a text message
        public static async Task<JsonResult> SendMessageText(string mobileNumber, string message_text)
        {
            if (platform == 1)
            {
                await TelegramWalletApiController.SendMessageText(mobileNumber, message_text);
            }

            if (platform == 2)
            {
                message_text = message_text.Replace("&", "AND"); //whatsapp does not like ampersand symbol
                await GupshupWalletApiController.SendMessageText(mobileNumber, message_text);
            }

            return new JsonResult(new { });
        }

        //send text message with image
        public static async Task<JsonResult> SendMessageImageAndText(string mobileNumber, string image_content,
            string caption)
        {
            if (platform == 1)
            {
                await TelegramWalletApiController.SendMessageImageAndText(mobileNumber, image_content, caption);
            }

            if (platform == 2)
            {
                caption = caption.Replace("&", "AND"); //whatsapp does not like ampersand symbol
                await GupshupWalletApiController.SendMessageImageAndText(mobileNumber, image_content,
                    HttpUtility.HtmlEncode(caption));
            }

            return new JsonResult(new { });
        }

        //send an error text message
        public static async Task<JsonResult> SendErrorMessageText(string mobileNumber, string message_text)
        {
            //if (platform == 1) await TelegramWalletApiController.SendErrorMessageText(mobileNumber, message_text);
            //if (platform == 2) await GupshupWalletApiController.SendErrorMessageText(mobileNumber, message_text);
            //send errot through telegram always
            await TelegramWalletApiController.SendErrorMessageText("586097924", message_text);
            return new JsonResult(new { });
        }
    }
}