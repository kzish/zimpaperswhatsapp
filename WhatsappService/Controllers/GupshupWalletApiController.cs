﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot;
using Models;
using WhatsappService.Models;
using System.IO;
using System.Net.Http.Headers;

namespace WalletApi.Controllers
{
    /// <summary>
    /// the telegram wallet api
    /// </summary>
    public class GupshupWalletApiController : Controller
    {
        private static HttpClient _client;

        private static HttpClient Client
        {
            get
            {
                if (_client != null) return _client;

                _client = new HttpClient();
                _client.DefaultRequestHeaders.Add("Accept", "application/x-www-form-urlencoded");
                _client.DefaultRequestHeaders.Add("apikey", $"{Globals.gupshup_apikey}");
                _client.DefaultRequestHeaders.Add("cache-control", "no-cache");
                _client.DefaultRequestHeaders.Add("Cache-Control", "no-cache");

                return _client;
            }
        }
        
        //receive message callback
        [HttpPost("RecieveMessageCallBack")]
        public void RecieveMessageCallBack()
        {
            using (var reader = new StreamReader(Request.Body))
            {
                try
                {
                    var message = new RecieveMessageCallBack();
                    string body = reader.ReadToEnd();

                    //using (var db = new dbContext())
                    //{
                    //    var log = new Logs();
                    //    log.Data1 = "callback";
                    //    log.Data2 = body;
                    //    log.Date = DateTime.Now;
                    //    db.Logs.Add(log);
                    //    db.SaveChanges();
                    //}

                    dynamic json_data = JsonConvert.DeserializeObject(body);
                    message.from = json_data.payload.source;
                    message.content = json_data.payload.payload.text;
                    WhatsAppWalletApiController.RecieveMessageCallBack(message);
                }
                catch (Exception e)
                {
                    Globals.logger.LogError(e.Message);
                }
            }
        }

        //send a text message
        public static async Task<bool> SendMessageText(string mobileNumber, string message_text)
        {
            try
            {
                var string_content = new StringContent(
                    $"channel=whatsapp&source={Globals.gupshup_source}&destination={mobileNumber}&message={System.Web.HttpUtility.UrlEncode(message_text)}&src.name=RubiWallet"
                    , Encoding.UTF8,
                    "application/x-www-form-urlencoded");

                var response = await Client.PostAsync($"{Globals.gupshup_end_point}", string_content).Result.Content
                    .ReadAsStringAsync();

                return true;
            }
            catch (Exception ex)
            {
                Globals.logger.LogError(ex.Message);
                return false;
            }
        }

        //send a text message rewrite
        public static async Task<bool> NewSendMessageText(string mobileNumber, string messageText)
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Content-Type", "application/x-www-form-urlencoded");
                client.DefaultRequestHeaders.Add("apikey", $"{Globals.gupshup_apikey}");

                var data = new Dictionary<string, string>
                {
                    {"channel", "whatsapp"},
                    {"source", Globals.gupshup_source},
                    {"destination", mobileNumber},
                    {"src.name", "RubiWallet"},
                    {"message", messageText}
                };

                var content = JsonConvert.SerializeObject(data);
                var stringContent = new StringContent(content, Encoding.UTF8, "application/x-www-form-urlencoded");

                var response = await client.PostAsync($"{Globals.gupshup_end_point}", stringContent).Result.Content
                    .ReadAsStringAsync();
                
                //log only for testing
                using (var db = new dbContext())
                {
                    var log = new Logs {Data1 = "response", Data2 = response, Date = DateTime.Now};
                    db.Logs.Add(log);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                using (var db = new dbContext())
                {
                    var log = new Logs {Data1 = ex.Message, Data2 = ex.StackTrace, Date = DateTime.Now};
                    db.Logs.Add(log);
                    db.SaveChanges();
                }
                Globals.logger.LogError(ex.Message);
                return false;
            }
        }


        //send text message with image
        public static async Task<bool> SendMessageImageAndText(string mobileNumber, string image_url, string caption)
        {
            try
            {
                dynamic json = new JObject();
                json.type = "image";
                json.previewUrl = image_url;
                json.originalUrl = image_url;
                json.caption = caption;

                string json_message = JsonConvert.SerializeObject(json);
                var string_content = new StringContent(
                    $"channel=whatsapp&source={Globals.gupshup_source}&destination={mobileNumber}&message={System.Web.HttpUtility.UrlEncode(json_message)}"
                    , Encoding.UTF8,
                    "application/x-www-form-urlencoded");

                var response = await Client.PostAsync($"{Globals.gupshup_end_point}", string_content).Result.Content
                    .ReadAsStringAsync();

                return true;
            }
            catch (Exception ex)
            {
                Globals.logger.LogError(ex.Message);
                return false;
            }
        }

        //send text message with image rewrite
        public static async Task<bool> NewSendMessageImageAndText(string mobileNumber, string imageUrl, string caption)
        {
            try
            {
                var client = new HttpClient();
                var request = new HttpRequestMessage(HttpMethod.Post, $"{Globals.gupshup_end_point}");
                
                client.DefaultRequestHeaders.Add("Accept", "application/x-www-form-urlencoded");
                client.DefaultRequestHeaders.Add("apikey", $"{Globals.gupshup_apikey}");

                dynamic message = new JObject();
                message.type = "image";
                message.originalUrl = imageUrl;
                message.previewUrl = imageUrl;
                message.caption = caption;
                
                dynamic data = new JObject();
                data.channel = "whatsapp";
                data.source = Globals.gupshup_source;
                data.destination = mobileNumber;
                data.message = message;
                
                HttpContent httpContent;
                
                var ms = new MemoryStream();
                var sw = new StreamWriter(ms, new UTF8Encoding(false), 1024, true);
                var jtw = new JsonTextWriter(sw) {Formatting = Formatting.None};
                var js = new JsonSerializer();
                js.Serialize(jtw, data);
                jtw.Flush();
                
                ms.Seek(0, SeekOrigin.Begin);
                httpContent = new StreamContent(ms);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                
                request.Content = httpContent;
                var response = await client
                    .SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(false);
                
                //log only for testing
                using (var db = new dbContext())
                {
                    var log = new Logs {Data1 = "response", Data2 = response.ToString(), Date = DateTime.Now};
                    db.Logs.Add(log);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                using (var db = new dbContext())
                {
                    var log = new Logs {Data1 = ex.Message, Data2 = ex.StackTrace, Date = DateTime.Now};
                    db.Logs.Add(log);
                    db.SaveChanges();
                }
                Globals.logger.LogError(ex.Message);
                return false;
            }
        }

        
        //send an error text message
        public static async Task<bool> SendErrorMessageText(string mobileNumber, string message_text)
        {
            return await SendMessageText(mobileNumber, message_text);
        }

        //GET /statistics
        [HttpGet("statistics")]
        public ActionResult Statistics()
        {
            var db = new dbContext();
            try
            {
                var data = new Dictionary<string, int>();
                var subs = db.MSubscription
                    .AsQueryable();
                
                data.Add("totalCumulativeSubscriptions", subs.Count());
                
                data.Add("totalMonthlySubscriptions", subs
                    .Count(i => i.DateStart >= new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)));
                
                data.Add("totalDailySubscriptions", subs.Count(i => i.DateStart >= DateTime.Today));
                
                data.Add("totalCumulativeActivations", subs
                    .Select(i => i.WhatsAppMobileNumber)
                    .Distinct().Count());
                
                data.Add("totalMonthlyActivations", subs
                    .Where(i => i.DateStart >= new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1))
                    .Select(i => i.WhatsAppMobileNumber)
                    .Distinct()
                    .Except(
                        subs
                            .Where(i => i.DateStart < new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1))
                            .Select(i => i.WhatsAppMobileNumber)
                            .Distinct()
                    ) // remove those that have subscribed before, to get new activations
                    .Count());
                
                data.Add("totalDailyActivations", subs
                    .Where(i => i.DateStart >= DateTime.Today)
                    .Select(i => i.WhatsAppMobileNumber)
                    .Distinct()
                    .Except(
                        subs
                            .Where(i => i.DateStart < DateTime.Today)
                            .Select(i => i.WhatsAppMobileNumber)
                            .Distinct()
                    ) // remove those that have subscribed before, to get new activations
                    .Count());
                
                data.Add("hardLogoChurnToday", db.Logs
                    .Where(i => i.Data1.ToString() == "churn")
                    .Count(i => i.Date >= DateTime.Today));
                
                data.Add("totalLogoChurn", subs
                    .Where(i => i.DateEnd < DateTime.Today) // expired subscription
                    .Select(i => i.WhatsAppMobileNumber)
                    .Distinct()
                    .Except(
                        subs.Where(i => i.DateEnd >= DateTime.Now) // remove those with valid subscriptions
                            .Select(i => i.WhatsAppMobileNumber)
                            .Distinct()
                    )
                    .Count());
                
                data.Add("totalDailyReaders", subs
                    .Where(i => i.DateEnd >= DateTime.Now)
                    .Select(i => i.WhatsAppMobileNumber)
                    .Distinct()
                    .Intersect(
                        db.MSession
                            .Where(i => i.Date >= DateTime.Today)
                            .Select(i => i.WhatsAppMobileNumber)
                            .Distinct()
                        )
                    .Count());
                
                data.Add("totalInactiveLogos", subs
                    .Where(i => i.DateEnd >= DateTime.Now) //valid subscription
                    .Select(i => i.WhatsAppMobileNumber)
                    .Distinct()
                    .Intersect(
                        db.MSession // have expired session
                            .Where(i => i.Date < DateTime.Now.AddHours(-Globals.SessionValidityPeriod))
                            .Select(i => i.WhatsAppMobileNumber)
                            .Distinct()
                    )
                    .Count());
                
                return Ok(data);
            }
            catch (Exception e)
            {
                Globals.logger.LogError(e.Message);
                return StatusCode(500, e);
            }
            finally
            {
                db.Dispose();
            }
        }
        
        //GET /subscribersList
        [HttpGet("subscribersList")]
        public ActionResult SubscribersList()
        {
            var db = new dbContext();
            try
            {
                var data = new Dictionary<string, List<string>>();
                var subs = db.MSubscription
                    .AsQueryable();
                
                data.Add("logoChurnList", subs
                    .Where(i => i.DateEnd < DateTime.Today) // expired subscription
                    .Select(i => i.WhatsAppMobileNumber)
                    .Distinct()
                    .Except(
                        subs.Where(i => i.DateEnd >= DateTime.Now) // remove those with valid subscriptions
                            .Select(i => i.WhatsAppMobileNumber)
                            .Distinct()
                    )
                    .ToList());
                
                data.Add("inactiveLogosList", subs
                    .Where(i => i.DateEnd >= DateTime.Now) //valid subscription
                    .Select(i => i.WhatsAppMobileNumber)
                    .Distinct()
                    .Intersect(
                        db.MSession // have expired session
                            .Where(i => i.Date < DateTime.Now.AddHours(-Globals.SessionValidityPeriod))
                            .Select(i => i.WhatsAppMobileNumber)
                            .Distinct()
                    )
                    .ToList());
                return Ok(data);
            }
            catch (Exception e)
            {
                Globals.logger.LogError(e.Message);
                return StatusCode(500, e);
            }
            finally
            {
                db.Dispose();
            }
        }
    }
}