﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot;
using Models;

namespace WalletApi.Controllers
{
    /// <summary>
    /// the telegram wallet api
    /// </summary>
    public class GupshupWalletApiController : Controller
    {
        private static HttpClient _client;

        private static HttpClient Client
        {
            get
            {
                if (_client != null) return _client;

                _client = new HttpClient();
                _client.DefaultRequestHeaders.Add("Accept", "application/x-www-form-urlencoded");
                _client.DefaultRequestHeaders.Add("apikey", $"{Globals.gupshup_apikey}");
                _client.DefaultRequestHeaders.Add("cache-control", "no-cache");
                _client.DefaultRequestHeaders.Add("Cache-Control", "no-cache");

                return _client;
            }
        }

        //receive message callback
        [HttpPost("RecieveMessageCallBack")]
        public void RecieveMessageCallBack([FromBody] GupShupRecieveMessageCallBack e)
        {
            var message = new RecieveMessageCallBack
            {
                content = e.payload.payload.text,
                from = e.payload.source
            };
            //this will act like the whatsapp mobile number (sender)
            WhatsAppWalletApiController.RecieveMessageCallBack(message);
        }

        //send a text message
        public static async Task<bool> SendMessageText(string mobileNumber, string messageText)
        {
            try
            {
                var string_content = new StringContent(
                    $"channel=whatsapp&source={Globals.gupshup_source}&destination={mobileNumber}&message={messageText}&src.name=RubiWallet"
                    , Encoding.UTF8,
                    "application/x-www-form-urlencoded"
                );

                var response = await Client.PostAsync($"{Globals.gupshup_end_point}", string_content).Result.Content
                    .ReadAsStringAsync();

                return true;
            }
            catch (Exception ex)
            {
                Globals.logger.LogError(ex.Message);
                return false;
            }
        }

        //send text message with image
        public static async Task<bool> SendMessageImageAndText(string mobileNumber, string image_url, string _caption)
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Accept", "application/x-www-form-urlencoded");
                client.DefaultRequestHeaders.Add("apikey", $"{Globals.gupshup_apikey}");
                client.DefaultRequestHeaders.Add("cache-control", "no-cache");
                client.DefaultRequestHeaders.Add("Cache-Control", "no-cache");

                dynamic json = new JObject();
                json.type = "image";
                json.previewUrl = image_url;
                json.originalUrl = image_url;
                json.caption = _caption;

                string json_message = JsonConvert.SerializeObject(json);
                var string_content = new StringContent(
                    $"channel=whatsapp&source={Globals.gupshup_source}&destination={mobileNumber}&message={json_message}"
                    , Encoding.UTF8,
                    "application/x-www-form-urlencoded");

                var response = await client.PostAsync($"{Globals.gupshup_end_point}", string_content).Result.Content
                    .ReadAsStringAsync();
                return true;
            }
            catch (Exception ex)
            {
                Globals.logger.LogError(ex.Message);
                return false;
            }
        }

        //send an error text message
        public static async Task<bool> SendErrorMessageText(string mobileNumber, string message_text)
        {
            return await SendMessageText(mobileNumber, message_text);
        }

        //send a text message template
        public static async Task<bool> SendMessageTextTemplate(string mobileNumber, string template_message)
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Accept", "application/x-www-form-urlencoded");
                client.DefaultRequestHeaders.Add("apikey", $"{Globals.gupshup_apikey}");
                client.DefaultRequestHeaders.Add("cache-control", "no-cache");
                client.DefaultRequestHeaders.Add("Cache-Control", "no-cache");

                //param2 = "hassan";
                dynamic json_message = new JObject();
                json_message.isHSM = true;
                json_message.type = "text";
                json_message.text = template_message;

                var json_message_string = JsonConvert.SerializeObject(json_message);

                var string_content = new StringContent(
                    $"channel=whatsapp&source={Globals.gupshup_source}&destination={mobileNumber}&message={json_message_string}"
                    , Encoding.UTF8,
                    "application/x-www-form-urlencoded");

                var response = await client.PostAsync($"{Globals.gupshup_end_point}", string_content).Result.Content
                    .ReadAsStringAsync();
                if (!response.Contains("submitted"))
                {
                    WhatsAppWalletApiController.SendErrorMessageText("", response).Wait();
                }

                return true;
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
                return false;
            }
        }
    }
}