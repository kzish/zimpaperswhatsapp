﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

public class Globals
{
    public static string smartpay_end_point = "http://3.130.10.197:17000";

    //live 
    public static string gupshup_end_point = "https://api.gupshup.io/sm/api/v1/msg";
    public static string gupshup_apikey = "3cab8b6156a4454ec5a6cbd6b00d528c";
    public static string gupshup_source = "263719004434";
    public static string src_name = "rubiem";

    public static string ecocash_username = "scb";
    public static string ecocash_password = "a!hksrtsb";

    //for images //must point to admin because this is where the images are /wwwroot/uploads
    public static string base_path = "https://rubieminnovations.com:17005/Admin";

    //1=telegram
    //2=gupshup
    public const int Platform = 2;

    //for local
//    public const string DbContextConnectionString =
//        "User ID=root;Password=password;Server=TERRENCE\\SQLEXPRESS;Initial Catalog=zimpapers;";

    //for production
    public const string DbContextConnectionString =
        @"server=.\SQLEXPRESS;database=zimpapers;User Id=sa;Password=gZi3hg8dn210Q;";

    private static ILoggerFactory loggerFactory = new LoggerFactory().AddConsole();
    public static ILogger logger = loggerFactory.CreateLogger<Globals>();
    
    // max number of headlines to show for a paper
    public const int LatestHeadlinesQty = 15; 
    
    public const int SessionValidityPeriod = 24; //hours
}