﻿using System;
using System.Collections.Generic;

namespace Admin.Models
{
    public partial class MDownloadable
    {
        public int Id { get; set; }
        public int PaperId { get; set; }
        public string WhatsAppMobileNumber { get; set; }
        public string FilePath { get; set; }
        public string Password { get; set; }

        public virtual MPapers Paper { get; set; }
    }
}
