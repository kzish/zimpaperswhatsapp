﻿using System;
using System.Collections.Generic;

namespace Admin.Models
{
    public partial class EPaperStatus
    {
        public EPaperStatus()
        {
            MPapers = new HashSet<MPapers>();
        }

        public int Id { get; set; }
        public string Status { get; set; }

        public virtual ICollection<MPapers> MPapers { get; set; }
    }
}
