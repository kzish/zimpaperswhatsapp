﻿using System;
using System.Collections.Generic;

namespace Admin.Models
{
    public partial class MBreakingNews
    {
        public int Id { get; set; }
        public string Headline { get; set; }
        public string Summary { get; set; }
        public string ImageUrl { get; set; }
        public DateTime DatePosted { get; set; }
        public bool Published { get; set; }
        public int PaperType { get; set; }

        public virtual EPaperType PaperTypeNavigation { get; set; }
    }
}
