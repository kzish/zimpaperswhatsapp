﻿using System;
using System.Collections.Generic;

namespace Admin.Models
{
    public partial class MSettings
    {
        public int Id { get; set; }
        public decimal SubscriptionDailyPrice { get; set; }
        public decimal SubscriptionWeeklyPrice { get; set; }
        public decimal SubscriptionMonthlyPrice { get; set; }
    }
}
