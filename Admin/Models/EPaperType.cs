﻿using System;
using System.Collections.Generic;

namespace Admin.Models
{
    public partial class EPaperType
    {
        public EPaperType()
        {
            MBreakingNews = new HashSet<MBreakingNews>();
            MHeadlines = new HashSet<MHeadlines>();
            MPapers = new HashSet<MPapers>();
            MSubscription = new HashSet<MSubscription>();
        }

        public int Id { get; set; }
        public string PaperType { get; set; }
        public decimal PurchasePrice { get; set; }
        public decimal SubscriptionDailyPrice { get; set; }
        public decimal SubscriptionWeeklyPrice { get; set; }
        public decimal SubscriptionMonthlyPrice { get; set; }

        public virtual ICollection<MBreakingNews> MBreakingNews { get; set; }
        public virtual ICollection<MHeadlines> MHeadlines { get; set; }
        public virtual ICollection<MPapers> MPapers { get; set; }
        public virtual ICollection<MSubscription> MSubscription { get; set; }
    }
}
