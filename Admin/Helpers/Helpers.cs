﻿using System;
using Admin.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting.Internal;


namespace Admin.Helpers
{
    public class Helpers
    {
        public static HostingEnvironment Host;

        /// <summary>
        /// return a dynamic object of the session_data.Data
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static dynamic ReadSessionData(string phone)
        {
            var db = new dbContext();
            try
            {
                var sessionData = db.MSession.Find(phone);
                dynamic data = JsonConvert.DeserializeObject(sessionData.Data);
                return data;
            }
            catch (Exception ex)
            {
                Globals.logger.LogError(ex.Message);
                return null;
            }
            finally
            {
                db.Dispose();
            }
        }
        
        /// <summary>
        /// inserts or updates a key value pair into the session_data.Data
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool WriteSessionData(string phone, string key, string value)
        {
            var db = new dbContext();
            try
            {
                var sessionData = db.MSession.Find(phone);

                dynamic data = !string.IsNullOrEmpty(sessionData.Data) ? JsonConvert.DeserializeObject(sessionData.Data) : new JObject();
                
                data[key] = value;//add the key and value
                sessionData.Data = JsonConvert.SerializeObject(data);//convert back to string
                db.Entry(sessionData).State = EntityState.Modified;//save the session entry
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Globals.logger.LogError(ex.Message);
                return false;
            }
            finally
            {
                db.Dispose();
            }
        }
    }
}