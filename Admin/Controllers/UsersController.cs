﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Admin.Controllers
{
    [Route("Users")]
    public class UsersController : Controller
    {
        private dbContext db = new dbContext();

        [HttpGet("AllBlockedUsers")]
        public IActionResult AllBlockedUsers()
        {
            ViewBag.title = "Blocked Users";
            return View();
        }


        [HttpGet("ajaxAllBlockedUsers")]
        public IActionResult ajaxAllBlockedUsers()
        {
            var users = db.MSession.Where(i => i.Blocked).ToList();
            ViewBag.users = users;
            return View();
        }

        [HttpPost("UnBlockUser")]
        public IActionResult UnBlockUser(string mobile)
        {
            try
            {
                var session_data = db.MSession.Find(mobile);
                dynamic data = null;
                //
                if (!string.IsNullOrEmpty(session_data.Data))
                    data = JsonConvert.DeserializeObject(session_data.Data);//get the json data
                else
                    data = new JObject();
                //
                data.error_count = 0;//add the key and value
                session_data.Data = JsonConvert.SerializeObject(data);//convert back to string
                session_data.Blocked = false;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] ="User has been unblocked";
                return RedirectToAction("AllBlockedUsers");
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("AllBlockedUsers");
            }
        }
    }
}
