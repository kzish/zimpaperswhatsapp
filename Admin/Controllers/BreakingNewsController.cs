﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Admin.Models;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WalletApi.Controllers;
using Microsoft.Extensions.Logging;

namespace Admin.Controllers
{
    [Route("BreakingNews")]
    public class BreakingNewsController : Controller
    {
        private HostingEnvironment host;
        dbContext db = new dbContext();

        public BreakingNewsController(HostingEnvironment host)
        {
            this.host = host;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [Route("AllBreakingNews/{paper_type_id}")]
        [Route("")]
        public IActionResult AllBreakingNews(int paper_type_id)
        {
            var paper_type = db.EPaperType.Find(paper_type_id);
            ViewBag.paper_type = paper_type;
            ViewBag.title = paper_type.PaperType + " BreakingNews";
            return View();
        }

        [Route("ArchivedBreakingNews/{paper_type_id}")]
        public IActionResult ArchivedBreakingNews(int paper_type_id)
        {
            var paperType = db.EPaperType.Find(paper_type_id);
            ViewBag.paper_type = paperType;
            ViewBag.title = paperType.PaperType + " Archived BreakingNews";
            return View();
        }

        [Route("ajaxAllBreakingNews/{paper_type_id}")]
        public IActionResult ajaxAllBreakingNews(int paper_type_id)
        {
            var BreakingNews = db.MBreakingNews
                .Where(i => i.PaperType == paper_type_id)
                .Where(i => i.DatePosted >= DateTime.Now.AddHours(-24))
                .ToList();
            ViewBag.BreakingNews = BreakingNews;
            return View();
        }

        [Route("ajaxArchivedBreakingNews/{paper_type_id}")]
        public IActionResult ajaxArchivedBreakingNews(int paper_type_id)
        {
            var breakingNews = db.MBreakingNews
                .Where(i => i.PaperType == paper_type_id)
                .Where(i => i.DatePosted < DateTime.Now.AddHours(-24))
                .ToList();
            ViewBag.BreakingNews = breakingNews;
            return View();
        }

        [HttpPost("DeleteBreakingNews")]
        public IActionResult DeleteBreakingNews(int id)
        {
            var BreakingNews = db.MBreakingNews.Find(id);
            try
            {
                if (BreakingNews != null) db.Remove(BreakingNews);

                if (!String.IsNullOrEmpty(BreakingNews.ImageUrl))
                {
                    System.IO.File.Delete(host.WebRootPath + "/uploads/" + BreakingNews.ImageUrl);
                }

                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
                return RedirectToAction("AllBreakingNews", "BreakingNews",
                    new {paper_type_id = BreakingNews.PaperType});
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("AllBreakingNews", "BreakingNews",
                    new {paper_type_id = BreakingNews.PaperType});
            }
        }

        [HttpGet("CreateBreakingNews/{paper_type_id}")]
        public IActionResult CreateBreakingNews(int paper_type_id)
        {
            var paper_type = db.EPaperType.Find(paper_type_id);
            ViewBag.title = "Create BreakingNews: " + paper_type.PaperType;
            ViewBag.paper_type = paper_type;
            return View();
        }

        [HttpPost("CreateBreakingNews")]
        public IActionResult CreateBreakingNews(MBreakingNews _BreakingNews, IFormFile image)
        {
            ViewBag.title = "Create BreakingNews";
            try
            {
                if (image != null)
                {
                    var filename = $"{Guid.NewGuid().ToString()}.{Path.GetExtension(image.FileName)}";
                    var filepath = $"{this.host.WebRootPath}/uploads/{filename}";
                    using (var stream = System.IO.File.Create(filepath))
                    {
                        image.CopyToAsync(stream).Wait();
                        stream.Dispose();
                    }

                    _BreakingNews.ImageUrl = filename;
                }

                _BreakingNews.DatePosted = DateTime.Now;
                db.MBreakingNews.Add(_BreakingNews);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["type"] = "Saved";
                return RedirectToAction("AllBreakingNews", "BreakingNews",
                    new {paper_type_id = _BreakingNews.PaperType});
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["type"] = ex.Message;
                return RedirectToAction("AllBreakingNews");
            }
        }

        [HttpGet("ViewBreakingNews/{id}")]
        public IActionResult ViewBreakingNews(int id)
        {
            ViewBag.title = "View BreakingNews";
            var BreakingNews = db.MBreakingNews.Find(id);
            ViewBag.BreakingNews = BreakingNews;
            return View();
        }

        [HttpPost("EditBreakingNews")]
        public IActionResult EditBreakingNews(MBreakingNews _BreakingNews, IFormFile image)
        {
            try
            {
                var old_BreakingNews = db.MBreakingNews.Find(_BreakingNews.Id);
                old_BreakingNews.Headline = _BreakingNews.Headline;
                old_BreakingNews.Summary = _BreakingNews.Summary;
                //old_BreakingNews.ImageUrl = _BreakingNews.null;
                //old_BreakingNews.DatePosted = _BreakingNews.DatePosted;
                old_BreakingNews.PaperType = _BreakingNews.PaperType;
                old_BreakingNews.Published = false; //if updated admin must be able to send this again to clients
                //

                //update image and remove old image
                if (image != null)
                {
                    if (!string.IsNullOrEmpty(old_BreakingNews.ImageUrl))
                    {
                        System.IO.File.Delete(this.host.WebRootPath + "/uploads/" + old_BreakingNews.ImageUrl);
                    }

                    var filename = $"{Guid.NewGuid().ToString()}.{Path.GetExtension(image.FileName)}";
                    var filepath = $"{this.host.WebRootPath}/uploads/{filename}";
                    using (var stream = System.IO.File.Create(filepath))
                    {
                        image.CopyToAsync(stream).Wait();
                        stream.Dispose();
                    }

                    old_BreakingNews.ImageUrl = filename;
                }

                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
                return RedirectToAction("AllBreakingNews", "BreakingNews",
                    new {paper_type_id = _BreakingNews.PaperType});
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("ViewBreakingNews", "BreakingNews", new {id = _BreakingNews.Id});
            }
        }

        [HttpPost("RemoveImage")]
        public IActionResult RemoveImage(int id)
        {
            try
            {
                var BreakingNews = db.MBreakingNews.Find(id);
                if (!string.IsNullOrEmpty(BreakingNews.ImageUrl))
                {
                    System.IO.File.Delete(this.host.WebRootPath + "/uploads/" + BreakingNews.ImageUrl);
                    BreakingNews.ImageUrl = null;
                    db.SaveChanges();
                }

                TempData["type"] = "success";
                TempData["msg"] = "Removed image";
                return RedirectToAction("ViewBreakingNews", "BreakingNews", new {id});
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("ViewBreakingNews", "BreakingNews", new {id});
            }
        }

        //push to clients
        //publish to all subscribers
        [HttpPost("PublishBreakingNews")]
        public IActionResult PublishBreakingNews(int breaking_news_id)
        {
            var breaking_news = db.MBreakingNews.Find(breaking_news_id);

            if (breaking_news == null) return NotFound();

            try
            {
                var valid_subscribers_for_this_news = db.MSubscription
                    .Where(i => i.DateEnd >= DateTime.Now.Date) //still valid subscription
                    .Where(i => i.EPaperType == breaking_news.PaperType) //this paper type
                    .Include(i => i.EPaperTypeNavigation)
                    .ToList();

                var template_message =
                    System.IO.File.ReadAllText(host.WebRootPath + "/Raw/BreakingNewsTemplate.txt");

                var summaryPreview = breaking_news.Summary.Length < 200
                    ? breaking_news.Summary.Trim()
                    : breaking_news.Summary.Substring(0, 200).Trim();

                var msg = template_message
                    .Replace("{{headline}}", breaking_news.Headline.Trim())
                    .Replace("{{paper_type}}", breaking_news.PaperTypeNavigation.PaperType.Trim())
                    .Replace("{{date}}", DateTime.Now.ToString("d-MMM-yyyy"))
                    .Replace("{{summary}}", summaryPreview);
                    //.Replace("{{link}}", "https://wa.me/263719004434?text=breakingx" + breaking_news.Id.ToString());

                //save before sending
                breaking_news.Published = true;
                db.SaveChanges();

                Task.Run(() => SendMessages(valid_subscribers_for_this_news, msg));

                TempData["type"] = "success";
                TempData["msg"] = "Published";
                return RedirectToAction("AllBreakingNews", "BreakingNews",
                    new {paper_type_id = breaking_news.PaperType});
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("AllBreakingNews", "BreakingNews",
                    new {paper_type_id = breaking_news.PaperType});
            }
        }

        private static async Task SendMessages(IEnumerable<MSubscription> subscribers,
            string message)
        {
//            await Task.Delay(new TimeSpan(0, 0, 2, 0));    //wait to simulate 1000 sending time
            foreach (var subscriber in subscribers)
            {
                try
                {
                    await WhatsAppWalletApiController.SendMessageText(subscriber.WhatsAppMobileNumber, message);
                }
                catch (Exception ex)
                {
                    Globals.logger.LogError(ex.Message);
                }
            }
        }
    }
}