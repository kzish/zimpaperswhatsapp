﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Admin.Models;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Admin.Controllers
{
    [Route("Articles")]
    public class ArticlesController : Controller
    {
        dbContext db = new dbContext();

        private HostingEnvironment host;

        public ArticlesController(HostingEnvironment host)
        {
            this.host = host;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [HttpGet("ajaxFetchArticles")]
        public IActionResult ajaxFetchArticles(int article_type_id, int paper_id)
        {
            var articles = db.MArticle
                .Where(i => i.EArticleType == article_type_id)
                .Where(i => i.PaperId == paper_id)
                .Include(i => i.EArticleTypeNavigation)
                .OrderByDescending(i => i.Id)
                .ToList();
            ViewBag.articles = articles;

            return View();
        }


        [HttpPost("DeleteArticle")]
        public IActionResult DeleteArticle(int paper_id, int article_id)
        {
            try
            {
                var article = db.MArticle.Find(article_id);
                if (article != null) db.Remove(article);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
                return RedirectToAction("ViewPaper", "Papers", new { paper_id = paper_id });
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("ViewPaper", "Papers", new { paper_id = paper_id });
            }
        }

        [HttpGet("CreateArticle/{paper_id}")]
        public IActionResult CreateArticle(int paper_id)
        {
            ViewBag.title = "Create Article";
            ViewBag.paper_id = paper_id;
            var article_types = db.EArticleType.ToList();
            ViewBag.article_types = article_types;
            return View();
        }

        [HttpPost("CreateArticle")]
        public async Task<IActionResult> CreateArticle(MArticle article,IFormFile _ArticleImage)
        {
            try
            {
                if (_ArticleImage != null)
                {
                    //save image
                    var filename = $"{Guid.NewGuid().ToString()}.{Path.GetExtension(_ArticleImage.FileName)}";
                    var filepath = $"{this.host.WebRootPath}/uploads/{filename}";
                    using (var stream = System.IO.File.Create(filepath))
                    {
                        await _ArticleImage.CopyToAsync(stream);
                        stream.Dispose();
                    }
                    article.ArticleImage = filename;
                }

                db.MArticle.Add(article);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
                return RedirectToAction("ViewPaper", "Papers", new { paper_id = article.PaperId });
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("ViewPaper", "Papers", new { paper_id = article.PaperId });
            }


        }

        [HttpGet("EditArticle/{id}")]
        public IActionResult EditArticle(int id)
        {
            ViewBag.title = "Edit Article";
            var article = db.MArticle.Find(id);
            ViewBag.article = article;

            var article_types = db.EArticleType.ToList();
            ViewBag.article_types = article_types;

            return View();
        }


        [HttpPost("EditArticle")]
        public async Task<IActionResult> EditArticle(MArticle article, IFormFile _ArticleImage)
        {
            var paper_id = 0;
            try
            {
                var old_article = db.MArticle.Find(article.Id);
                paper_id = old_article.PaperId;
                old_article.EArticleType = article.EArticleType;
                old_article.ArticleTitle = article.ArticleTitle;
                old_article.ArticleBody = article.ArticleBody;


                if (_ArticleImage != null && !string.IsNullOrEmpty(old_article.ArticleImage))
                {
                    //delete old image
                    System.IO.File.Delete($"{this.host.WebRootPath}/uploads/{old_article.ArticleImage}");
                }
                if (_ArticleImage != null)
                {
                    //save new one
                    var filename = $"{Guid.NewGuid().ToString()}.{Path.GetExtension(_ArticleImage.FileName)}";
                    var filepath = $"{this.host.WebRootPath}/uploads/{filename}";
                    using (var stream = System.IO.File.Create(filepath))
                    {
                        await _ArticleImage.CopyToAsync(stream);
                        stream.Dispose();
                    }
                    old_article.ArticleImage = $"{filename}";
                }

                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
                return RedirectToAction("ViewPaper", "Papers", new { paper_id = paper_id });
            }
            catch (Exception ex)
            {
                System.IO.File.WriteAllText(@"c:\rubiem\error.txt", ex.Message);
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("ViewPaper", "Papers", new { paper_id = paper_id });
            }


        }


        [HttpGet("ArticleTypes")]
        public IActionResult ArticleTypes()
        {
            ViewBag.title = "Article Types";
            var article_types = db.EArticleType
                .ToList();
            ViewBag.article_types = article_types;
            return View();
        }

        [HttpPost("DeleteArticleType")]
        public IActionResult DeleteArticleType(int id)
        {
            try
            {
                var article_type = db.EArticleType.Find(id);
                if (article_type != null) db.Remove(article_type);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
                return RedirectToAction("ArticleTypes");
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("ArticleTypes");
            }

        }


        [HttpPost("CreateArticleType")]
        public IActionResult CreateArticleType(EArticleType article_type)
        {
            ViewBag.title = "Create article type";
            try
            {
                db.EArticleType.Add(article_type);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["type"] = "Saved";
                return RedirectToAction("ArticleTypes");
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["type"] = ex.Message;
                return RedirectToAction("ArticleTypes");
            }
            finally
            {
            }
        }



    }
}
