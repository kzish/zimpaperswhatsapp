﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Admin.Models;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WalletApi.Controllers;

namespace Admin.Controllers
{

    [Route("Settings")]
    public class SettingsController : Controller
    {
        private HostingEnvironment host;
        dbContext db = new dbContext();

        public SettingsController(HostingEnvironment host)
        {
            this.host = host;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [HttpGet("PaperTypes")]
        public IActionResult PaperTypes()
        {
            ViewBag.title = "Paper Types";
            var paper_types = db.EPaperType
                .OrderByDescending(i => i.Id)
                .ToList();
            ViewBag.paper_types = paper_types;
            return View();
        }


        [HttpPost("UpdatePaperType")]
        public IActionResult UpdatePaperType(EPaperType paper_type)
        {
            try
            {
                //update and save paper type
                var old_paper_type = db.EPaperType.Find(paper_type.Id);
                old_paper_type.PaperType = paper_type.PaperType;
                old_paper_type.PurchasePrice = paper_type.PurchasePrice;
                old_paper_type.SubscriptionDailyPrice = paper_type.SubscriptionDailyPrice;
                old_paper_type.SubscriptionWeeklyPrice = paper_type.SubscriptionWeeklyPrice;
                old_paper_type.SubscriptionMonthlyPrice = paper_type.SubscriptionMonthlyPrice;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
                return RedirectToAction("PaperTypes");
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("PaperTypes");
            }
        }

        [HttpPost("DeletePaperType")]
        public IActionResult DeletePaperType(int id)
        {
            try
            {
                var paper_type = db.EPaperType.Find(id);
                if (paper_type != null) db.Remove(paper_type);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("AllSettings");


        }


        [HttpPost("CreatePaperType")]
        public IActionResult CreatePaperType(string paper_type)
        {
            ViewBag.title = "Create paper type";
            try
            {
                var exists = db.EPaperType.Where(i => i.PaperType == paper_type).Any();
                if(exists)
                {
                    TempData["type"] = "error";
                    TempData["msg"] = $"Paper Type {paper_type} Already Exists";
                    return RedirectToAction("AllSettings");
                }
                var PaperType = new EPaperType();
                PaperType.PaperType = paper_type;
                db.EPaperType.Add(PaperType);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("AllSettings");
        }


        [HttpPost("SetSubscriptionPrices")]
        public IActionResult SetSubscriptionPrices(decimal daily,decimal weekly,decimal monthly)
        {
            try
            {
                var settings = db.MSettings.First();
                settings.SubscriptionDailyPrice = daily;
                settings.SubscriptionWeeklyPrice = weekly;
                settings.SubscriptionMonthlyPrice = monthly;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] ="Saved";
            }
            catch(Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("AllSettings");

        }

        [HttpGet("AllSettings")]
        public IActionResult AllSettings()
        {
            ViewBag.title = "Settings";
            var paper_types = db.EPaperType.ToList();
            ViewBag.paper_types = paper_types;
            var settings = db.MSettings.First();
            ViewBag.settings = settings;
            return View();
        }

    }
}
