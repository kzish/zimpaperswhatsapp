﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

namespace SpeedLinkAdminPortal.Controllers
{
    [Route("Home")]
    [Route("")]
    [Authorize(Roles = "admin")]
    public class HomeController : Controller
    {
        dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        RoleManager<IdentityRole> roleManager;

        public HomeController(RoleManager<IdentityRole> roleManager)
        {
            this.roleManager = roleManager;
        }

        [Route("Dashboard")]
        [Route("")]
        public IActionResult Dashboard(string sub = "active")
        {
            ViewBag.title = "Dashboard";
            //TODO get month from view and select for that month
            var dailySubsDataForMonth = DateTime.Now.Month;

            var subs = db.MSubscription
                .Include(i => i.EPaperTypeNavigation)
                .AsQueryable();
            var totalCumulativeSubscriptions = subs.Count();
            
            var totalMonthlySubscriptions = subs
                .Count(i => i.DateStart >= new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
            
            var totalDailySubscriptions = subs.Count(i => i.DateStart >= DateTime.Today);
            
            var totalCumulativeActivations = subs
                .Select(i => i.WhatsAppMobileNumber)
                .Distinct().Count();
            
            var totalMonthlyActivations = subs
                .Where(i => i.DateStart >= new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1))
                .Select(i => i.WhatsAppMobileNumber)
                .Distinct()
                .Except(
                    subs
                        .Where(i => i.DateStart < new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1))
                        .Select(i => i.WhatsAppMobileNumber)
                        .Distinct()
                    )  // remove those that have subscribed before, to get new activations
                .Count();
            
            var totalDailyActivations = subs
                .Where(i => i.DateStart >= DateTime.Today)
                .Select(i => i.WhatsAppMobileNumber)
                .Distinct()
                .Except(
                    subs
                        .Where(i => i.DateStart < DateTime.Today)
                        .Select(i => i.WhatsAppMobileNumber)
                        .Distinct()
                )  // remove those that have subscribed before, to get new activations
                .Count();
            
            //hard logo churn - those that explicitly unsubscribed from the platform
            var hardLogoChurnToday = db.Logs
                .Where(i => i.Data1.ToString() == "churn")
                .Count(i => i.Date >= DateTime.Today);
            
            //logo churn - those who have expired subscriptions and have not subscribed again
            var totalLogoChurn = subs
                .Where(i => i.DateEnd < DateTime.Today) // expired subscription
                .Select(i => i.WhatsAppMobileNumber)
                .Distinct()
                .Except(
                    subs.Where(i => i.DateEnd >= DateTime.Now) // remove those with valid subscriptions
                        .Select(i => i.WhatsAppMobileNumber)
                        .Distinct()
                    )
                .Count();

            //get list of numbers in session today
            var whatsAppNumbersInSess = db.MSession
                .Where(i => i.Date >= DateTime.Today)
                .Select(i => i.WhatsAppMobileNumber)
                .ToList(); 
            // TODO use .Intersect()
            // If valid subscription && in session then they are more likely to have read news today
            var totalDailyReaders = subs
                .Where(i => i.DateEnd >= DateTime.Now)
                .Count(i => whatsAppNumbersInSess.Contains(i.WhatsAppMobileNumber));
            
            var totalInactiveLogos = subs
                .Where(i => i.DateEnd >= DateTime.Now)     //valid subscription
                .Select(i => i.WhatsAppMobileNumber)
                .Distinct()
                .Intersect(
                    db.MSession    // have expired session
                        .Where(i => i.Date < DateTime.Now.AddHours(-Globals.SessionValidityPeriod))  
                        .Select(i => i.WhatsAppMobileNumber)
                        .Distinct()
                    )
                .Count();
            
//            if (sub == "active")
//            {
//                subs = subs.Where(i => i.DateEnd >= DateTime.Now);
//            }

            var chart1 = string.Empty;
            var chart2 = string.Empty;

            var data = subs
                .GroupBy(i => i.EPaperTypeNavigation.PaperType)
                .Select(i => new
                {
                    name = i.Key,
                    y = i.Count()
                }).ToList();
            
            var  subsForMonthOf = dailySubsDataForMonth == DateTime.Now.Month ? subs.Where(i => i.DateStart.Month == DateTime.Now.Month) 
                : subs.Where(i => i.DateStart.Month == dailySubsDataForMonth);
            
            var dailySubsData = subsForMonthOf
                .GroupBy(i => i.DateStart.Date)
                .Select(i => new
                {
                    name = i.Key,
                    y = i.Count()
                }).ToList();
            
            foreach (var item in data)
                chart1 += $"{{ name: '{item.name}', y: {item.y} }},";

            foreach (var item in dailySubsData)
                chart2 += $"['{(item.name.ToString().Replace("12:00:00 AM", ""))}', {item.y}],";

            ViewBag.sub = sub;
            ViewBag.chart1 = chart1;
            ViewBag.chart2 = chart2;
            ViewBag.totalCumulativeSubscriptions = totalCumulativeSubscriptions;
            ViewBag.totalMonthlySubscriptions = totalMonthlySubscriptions;
            ViewBag.totalDailySubscriptions = totalDailySubscriptions;
            ViewBag.totalDailyReaders = totalDailyReaders;
            ViewBag.totalCumulativeActivations = totalCumulativeActivations;
            ViewBag.totalMonthlyActivations = totalMonthlyActivations;
            ViewBag.totalDailyActivations = totalDailyActivations;
            ViewBag.hardLogoChurnToday = hardLogoChurnToday;
            ViewBag.totalLogoChurn = totalLogoChurn;
            ViewBag.totalInactiveLogos = totalInactiveLogos;

            return View();
        }

        [HttpGet("Messages")]
        public IActionResult Messages()
        {
            ViewBag.title = "Messages";
            return View();
        }

        [HttpGet("AllRoles")]
        public IActionResult AllRoles()
        {
            ViewBag.title = "All Roles";
            var roles = roleManager.Roles.ToList();
            ViewBag.roles = roles;
            return View();
        }
    }
}