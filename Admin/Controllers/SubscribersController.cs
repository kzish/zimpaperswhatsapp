﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Admin.Controllers
{
    [Route("Subscribers")]
    public class SubscribersController : Controller
    {
        dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [HttpGet("ajaxSubscribers")]
        public IActionResult ajaxSubscribers(string type="All")
        {
            var subscribers = db.MSubscription
                .AsQueryable()
                .Include(i => i.EPaperTypeNavigation)
                .OrderByDescending(i=>i.Id)
                ;
            ViewBag.subscribers = subscribers.ToList();
            return View();
        }

        [HttpGet("All")]
        public IActionResult All()
        {
            ViewBag.title = $"All Subscribers";
            return View();
        }

    }
}
