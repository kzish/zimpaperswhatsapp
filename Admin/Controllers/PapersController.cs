﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WalletApi.Controllers;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Admin.Controllers
{

    [Route("Papers")]
    public class PapersController : Controller
    {
        dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [Route("AllPapers/{paper_type_id}")]
        [Route("")]
        public IActionResult AllPapers(int paper_type_id)
        {
            var paper_type = db.EPaperType.Find(paper_type_id);
            ViewBag.paper_type = paper_type;
            ViewBag.title = paper_type.PaperType + " Papers";
            return View();
        }

        [Route("ajaxAllPapers/{paper_type_id}")]
        public IActionResult ajaxAllPapers(int paper_type_id)
        {
            var papers = db.MPapers
                .Where(i => i.EPaperType == paper_type_id)
                .Include(i => i.EPaperTypeNavigation)
                .Include(i => i.EPaperStatusNavigation)
                .Include(i => i.MArticle)
                .OrderByDescending(i => i.Id)
                .ToList();
            ViewBag.papers = papers;
            return View();
        }

        [HttpGet("PaperTypes")]
        public IActionResult PaperTypes()
        {
            ViewBag.title = "Paper Types";
            var paper_types = db.EPaperType
                .OrderByDescending(i => i.Id)
                .ToList();
            ViewBag.paper_types = paper_types;
            return View();
        }

        [HttpPost("DeletePaper")]
        public IActionResult DeletePaper(int id)
        {
            try
            {
                var paper = db.MPapers.Find(id);
                if (paper != null) db.Remove(paper);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
                return RedirectToAction("AllPapers");
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("AllPapers");
            }

        }

        [HttpPost("UpdatePaperType")]
        public IActionResult UpdatePaperType(EPaperType paper_type)
        {
            try
            {
                //update and save paper type
                var old_paper_type = db.EPaperType.Find(paper_type.Id);
                old_paper_type.PaperType = paper_type.PaperType;
                old_paper_type.PurchasePrice = paper_type.PurchasePrice;
                old_paper_type.SubscriptionDailyPrice = paper_type.SubscriptionDailyPrice;
                old_paper_type.SubscriptionWeeklyPrice = paper_type.SubscriptionWeeklyPrice;
                old_paper_type.SubscriptionMonthlyPrice = paper_type.SubscriptionMonthlyPrice;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
                return RedirectToAction("PaperTypes");
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("PaperTypes");
            }
        }

        [HttpPost("DeletePaperType")]
        public IActionResult DeletePaperType(int id)
        {
            try
            {
                var paper_type = db.EPaperType.Find(id);
                if (paper_type != null) db.Remove(paper_type);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
                return RedirectToAction("PaperTypes");
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("PaperTypes");
            }

        }

        [HttpGet("CreatePaper")]
        public IActionResult CreatePaper()
        {
            ViewBag.title = "Create Paper";

            var paper_types = db.EPaperType.ToList();
            ViewBag.paper_types = paper_types;

            var paper_statuses = db.EPaperStatus.ToList();
            ViewBag.paper_statuses = paper_statuses;

            return View();
        }
        
        [HttpPost("CreatePaper")]
        public IActionResult CreatePaper(MPapers paper)
        {
            ViewBag.title = "Create paper";
            try
            {
                paper.DateCreated = DateTime.Now;
                db.MPapers.Add(paper);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["type"] = "Saved";
                return RedirectToAction("AllPapers");
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["type"] = ex.Message;
                return RedirectToAction("AllPapers");
            }
            finally
            {
            }
        }

        [HttpPost("CreatePaperType")]
        public IActionResult CreatePaperType(EPaperType paper_type)
        {
            ViewBag.title = "Create paper type";
            try
            {
                db.EPaperType.Add(paper_type);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["type"] = "Saved";
                return RedirectToAction("PaperTypes");
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["type"] = ex.Message;
                return RedirectToAction("PaperTypes");
            }
            finally
            {
            }
        }
        
        [HttpGet("ViewPaper/{paper_id}")]
        public IActionResult ViewPaper(int paper_id)
        {
            ViewBag.title = "View paper";

            var paper = db.MPapers
                .Where(i => i.Id == paper_id)
                .Include(i => i.EPaperStatusNavigation)
                .Include(i => i.EPaperTypeNavigation)
                .FirstOrDefault();

            ViewBag.paper = paper;

            var article_types = db.EArticleType.ToList();
            ViewBag.article_types = article_types;

            var paper_types = db.EPaperType.ToList();
            ViewBag.paper_types = paper_types;

            var paper_statuses = db.EPaperStatus.ToList();
            ViewBag.paper_statuses = paper_statuses;



            return View();
        }
        
        [HttpPost("EditPaper")]
        public IActionResult EditPaper(MPapers paper)
        {
            try
            {
                var old_paper = db.MPapers.Find(paper.Id);
                old_paper.EPaperType = paper.EPaperType;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
                return RedirectToAction("ViewPaper", "Papers", new { paper_id = paper.Id });
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("ViewPaper", "Papers", new { paper_id = paper.Id });
            }
        }

        //send this paper to all subscribed clients
        [HttpPost("PushPaper")]
        public async Task<IActionResult> PushPaper(int id)
        {
            try
            {
                var paper = db.MPapers
                    .Include(i => i.MArticle)//include articles
                    .Where(i => i.Id == id)//this paper
                    .FirstOrDefault();

                var valid_subscribers = db.MSubscription
                    .Where(i => i.DateEnd >= DateTime.Now)//still valid subscription
                    .ToList();

                //push to all subscribers
                foreach (var sub in valid_subscribers)
                {
                    //push each article
                    foreach (var article in paper.MArticle)
                    {
                       // await WhatsAppWalletApiController.SendMessageTextTemplate(sub.WhatsAppMobileNumber, article.ArticleTitle, article.ArticleBody);
                    }
                }
                paper.Pushed = true;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Pushed Breaking News";
                return RedirectToAction("Allpapers");
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("Allpapers");
            }
        }

    }
}
