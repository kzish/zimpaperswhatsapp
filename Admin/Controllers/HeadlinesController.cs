﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Admin.Models;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WalletApi.Controllers;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Admin.Controllers
{
    [Route("Headlines")]
    public class HeadlinesController : Controller
    {
        private HostingEnvironment host;
        dbContext db = new dbContext();
        private static int array_limit = Globals.LatestHeadlinesQty;

        public HeadlinesController(HostingEnvironment host)
        {
            this.host = host;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [Route("AllHeadlines/{paper_type_id}")]
        [Route("")]
        public IActionResult AllHeadlines(int paper_type_id)
        {
            var paper_type = db.EPaperType.Find(paper_type_id);
            var alreadyPublished = db.Logs
                .Where(i => i.Date >= DateTime.Now.AddHours(-1)) //within the hour
                .Any(i => i.Data1.ToString() == "published"); //published log
            ViewBag.paper_type = paper_type;
            ViewBag.title = paper_type.PaperType + " Headlines";
            ViewBag.alreadyPublished = alreadyPublished;
            return View();
        }

        [Route("ArchiveHeadlines/{paper_type_id}")]
        public IActionResult ArchiveHeadlines(int paper_type_id)
        {
            var paperType = db.EPaperType.Find(paper_type_id);
            ViewBag.paper_type = paperType;
            ViewBag.title = paperType.PaperType + " Archived Headlines";
            return View();
        }

        [Route("ajaxAllHeadlines/{paper_type_id}")]
        public IActionResult ajaxAllHeadlines(int paper_type_id)
        {
            var headlines = db.MHeadlines
                .Where(i => i.PaperType == paper_type_id)
                .Where(i => i.DatePosted >= DateTime.Now.AddHours(-24))
                .ToList();
            ViewBag.headlines = headlines;
            return View();
        }
        
        [Route("ajaxArchivedHeadlines/{paper_type_id}")]
        public IActionResult ajaxArchivedHeadlines(int paper_type_id)
        {
            var headlines = db.MHeadlines
                .Where(i => i.PaperType == paper_type_id)
                .Where(i => i.DatePosted < DateTime.Now.AddHours(-24))
                .ToList();
            ViewBag.headlines = headlines;
            return View();
        }

        [HttpPost("DeleteHeadline")]
        public IActionResult DeleteHeadline(int id)
        {
            var headline = db.MHeadlines.Find(id);
            try
            {
                if (headline != null) db.Remove(headline);

                if (!String.IsNullOrEmpty(headline.ImageUrl))
                {
                    System.IO.File.Delete(host.WebRootPath + "/uploads/" + headline.ImageUrl);
                }

                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
                return RedirectToAction("AllHeadlines", "Headlines", new {paper_type_id = headline.PaperType});
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("AllHeadlines", "Headlines", new {paper_type_id = headline.PaperType});
            }
        }

        [HttpGet("CreateHeadline/{paper_type_id}")]
        public IActionResult CreateHeadline(int paper_type_id)
        {
            var paper_type = db.EPaperType.Find(paper_type_id);
            ViewBag.title = "Create headline: " + paper_type.PaperType;
            ViewBag.paper_type = paper_type;
            return View();
        }

        [HttpPost("CreateHeadline")]
        public IActionResult CreateHeadline(MHeadlines _headline, IFormFile image)
        {
            ViewBag.title = "Create Headline";
            try
            {
                if (image != null)
                {
                    var filename = $"{Guid.NewGuid().ToString()}.{Path.GetExtension(image.FileName)}";
                    var filepath = $"{this.host.WebRootPath}/uploads/{filename}";
                    using (var stream = System.IO.File.Create(filepath))
                    {
                        image.CopyToAsync(stream).Wait();
                        stream.Dispose();
                    }

                    _headline.ImageUrl = filename;
                }

                _headline.DatePosted = DateTime.Now;
                db.MHeadlines.Add(_headline);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["type"] = "Saved";
                return RedirectToAction("AllHeadlines", "Headlines", new {paper_type_id = _headline.PaperType});
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["type"] = ex.Message;
                return RedirectToAction("Dashboard", "Home");
            }
            finally
            {
            }
        }

        [HttpGet("ViewHeadline/{id}")]
        public IActionResult ViewHeadline(int id)
        {
            ViewBag.title = "View Headline";
            var headline = db.MHeadlines.Find(id);
            ViewBag.headline = headline;
            return View();
        }

        [HttpPost("EditHeadline")]
        public IActionResult EditHeadline(MHeadlines _headline, IFormFile image)
        {
            try
            {
                var old_headline = db.MHeadlines.Find(_headline.Id);
                old_headline.Headline = _headline.Headline;
                old_headline.Summary = _headline.Summary;
                //old_headline.ImageUrl = _headline.null;
                //old_headline.DatePosted = _headline.DatePosted;
                old_headline.Published = _headline.Published;
                old_headline.PaperType = _headline.PaperType;
                //

                //update image and remove old image
                if (image != null)
                {
                    if (!string.IsNullOrEmpty(old_headline.ImageUrl))
                    {
                        System.IO.File.Delete(this.host.WebRootPath + "/uploads/" + old_headline.ImageUrl);
                    }

                    var filename = $"{Guid.NewGuid().ToString()}.{Path.GetExtension(image.FileName)}";
                    var filepath = $"{this.host.WebRootPath}/uploads/{filename}";
                    using (var stream = System.IO.File.Create(filepath))
                    {
                        image.CopyToAsync(stream).Wait();
                        stream.Dispose();
                    }

                    old_headline.ImageUrl = filename;
                }

                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
                return RedirectToAction("AllHeadlines", "Headlines", new {paper_type_id = _headline.PaperType});
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("ViewHeadline", "Headlines", new {id = _headline.Id});
            }
        }

        [HttpPost("RemoveImage")]
        public IActionResult RemoveImage(int id)
        {
            try
            {
                var headline = db.MHeadlines.Find(id);
                if (!string.IsNullOrEmpty(headline.ImageUrl))
                {
                    System.IO.File.Delete(this.host.WebRootPath + "/uploads/" + headline.ImageUrl);
                    headline.ImageUrl = null;
                    db.SaveChanges();
                }

                TempData["type"] = "success";
                TempData["msg"] = "Removed image";
                return RedirectToAction("ViewHeadline", "Headlines", new {id});
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("ViewHeadline", "Headlines", new {id});
            }
        }
        
        //publish to all subscribers
        [HttpPost("PublishHeadlines")]
        public IActionResult PublishHeadlines(int newsId)
        {
            //list papers
            List<EPaperType> paperTypes;
            using (var dbContext = new dbContext())
            {
                paperTypes = dbContext.EPaperType.ToList();
                dbContext.Logs.Add(new Logs {Data1 = "published", Data2 = "PublishHeadlines Function run", Date = DateTime.Now});
                dbContext.SaveChanges();
                dbContext.Dispose();
            }
            var selectedPaper = paperTypes.FirstOrDefault(i => i.Id.Equals(newsId));
            try
            {
                if (selectedPaper != null)
                {
                    //get list of numbers with a valid session
                    var whatsAppNumbersInSess = db.MSession
                        .Where(i => i.Date >= DateTime.Now.AddHours(-Globals.SessionValidityPeriod))
                        .Select(i => i.WhatsAppMobileNumber)
                        .ToList(); 
                    // TODO use .Intersect()
                    //Get valid subscribers 
                    var validSubscribersForNews = db.MSubscription
                        .Where(i => i.DateEnd >= DateTime.Now.Date)//valid subscription
                        .Where(i => i.EPaperType == selectedPaper.Id)//this paper type
                        .Where(i => whatsAppNumbersInSess.Contains(i.WhatsAppMobileNumber)) //have valid session
                        .Include(i => i.EPaperTypeNavigation)
                        .ToList();

                    Task.Run(() => ShowHeadlinesOfSelectedPaperType(selectedPaper, validSubscribersForNews));
                    TempData["type"] = "success";
                    TempData["msg"] = "Published Headlines";
                }
                else
                {
                    TempData["type"] = "error";
                    TempData["msg"] = "Error in selecting Paper Type";
                }
                return RedirectToAction("AllHeadlines", "Headlines", new {paper_type_id = newsId});
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("AllHeadlines", "Headlines", new {paper_type_id = newsId});
            }
        }

        private async Task ShowHeadlinesOfSelectedPaperType(EPaperType selectedPaper, IEnumerable<MSubscription> validSubscribersForNews)
        {
            foreach (var subscriber in validSubscribersForNews)
            {
                var db = new dbContext();
                var sessionData = GetSessionData(subscriber.WhatsAppMobileNumber);

                EPaperType paperType;
                if (selectedPaper != null)
                {
                    paperType = selectedPaper;
                    Helpers.Helpers.WriteSessionData(subscriber.WhatsAppMobileNumber, "selected_paper_type_id",
                        paperType.Id.ToString());
                }
                else
                {
                    var sessData = Helpers.Helpers.ReadSessionData(subscriber.WhatsAppMobileNumber);
                    var selectedPaperTypeId = int.Parse($"{sessData.selected_paper_type_id}");
                    paperType = db.EPaperType.Find(selectedPaperTypeId);
                }
                
                //initialize available headlines for paper
                try
                {
                    var sessData = Helpers.Helpers.ReadSessionData(subscriber.WhatsAppMobileNumber);
                    var validHeadlineIds = JsonConvert.DeserializeObject<List<int>>($"{sessData.valid_headline_ids}");
                    //TODO should always be null so dont check for null, just write. Actually never deserialize
                    if (validHeadlineIds == null)
                    {
                        //get a list of ids of headlines of the selected paper
                        validHeadlineIds = new List<int>();
                        var tempHeadlines = db.MHeadlines
                            .Where(i => i.PaperType == paperType.Id) //for this paper type
                            .Where(i => i.DatePosted > DateTime.Now.AddHours(-24)) //posted within the last 24hrs
                            .OrderByDescending(i => i.Id) 
                            .Take(array_limit) // take only max latest
                            .ToList();
                        foreach (var headline in tempHeadlines)
                        {
                            validHeadlineIds.Add(headline.Id);
                        }
                        Helpers.Helpers.WriteSessionData(subscriber.WhatsAppMobileNumber, "valid_headline_ids", JsonConvert.SerializeObject(validHeadlineIds));
                    }
                }
                catch (Exception exception)
                {
                    Globals.logger.LogError(exception.Message);
                }
                
                try
                {
                    var menu = System.IO.File.ReadAllText(host.WebRootPath +
                                                             "/Raw/ShowHeadlinesOfSelectedPaperType.txt");
                    var headlines = db.MHeadlines
                        .Where(i => i.PaperType == paperType.Id) //for this paper type
                        .Where(i => i.DatePosted > DateTime.Now.AddHours(-24)) //posted within the last 24hrs
                        .OrderByDescending(i => i.Id) 
                        .Take(array_limit) // take only 10 latest
                        .ToList();
                    
                    var list = string.Empty;
                    var index = array_limit * sessionData.IndexCounter; //start here
                
                    foreach (var item in headlines)
                    {
                        index++; //start at 1
                        list += $"{index} - {item.Headline.Trim()}{Environment.NewLine}";
                    }
                    
                    //menu = menu.Replace("{{selected_paper}}", paperType.PaperType.Trim());
                    menu = menu.Replace("{{list}}", string.IsNullOrEmpty(list) ? $"No headlines at this time{Environment.NewLine}" : list);
                    await WhatsAppWalletApiController.SendMessageText(subscriber.WhatsAppMobileNumber, menu);

                    sessionData.Date = DateTime.Now;
                    sessionData.LastMenu = "GetLatestNews.PromptShowHeadlinesOfSelectedPaperType"; // so that user just enters headline number and receive news immediatly
                    db.SaveChanges(); //update session
                }
                catch (Exception exception)
                {
                    Globals.logger.LogError(exception.Message);
                    var log = new Logs {Data1 = exception.Message, Data2 = exception.StackTrace, Date = DateTime.Now};
                    db.Logs.Add(log);
                    db.SaveChanges();
                }
                finally
                {
                    db.Dispose();
                }
            }
        }

        //fetch the session data
        public static MSession GetSessionData(string phone)
        {
            var db = new dbContext();
            try
            {
                var sessionData = db.MSession.FirstOrDefault(i => i.WhatsAppMobileNumber == phone);

                if (sessionData == null)
                {
                    //if no session exists create new session and save it
                    sessionData = new MSession
                    {
                        WhatsAppMobileNumber = phone,
                        Date = DateTime.Now,
                        LastMenu = "GetLatestNews.PromptShowHeadlinesOfSelectedPaperType",
                        IndexCounter = 0
                    };
                    db.MSession.Add(sessionData);
                    db.SaveChanges();
                }
                else
                {
                    //clear sessions and restart for everyone because at publish time we want everyone to be 
                    //at the same menu i.e LastMenu = "GetLatestNews.PromptShowHeadlinesOfSelectedPaperType"
                    ClearMySession(sessionData);
                }

                return sessionData;
            }
            catch (Exception ex)
            {
                Globals.logger.LogError(ex.Message);
                return null;
            }
            finally
            {
                db.Dispose();
            }
        }
        
        //clear my session to start afresh
        private static void ClearMySession(MSession oldSession)
        {
            var db = new dbContext();
            var oldWhatsAppMobileNumber = oldSession.WhatsAppMobileNumber;

            try
            {
                //remove old session to clear session
                db.MSession.Remove(oldSession);

                //add new blank session
                var newSession = new MSession
                {
                    WhatsAppMobileNumber = oldWhatsAppMobileNumber, 
                    Date = DateTime.Now, 
                    LastMenu = "GetLatestNews.PromptShowHeadlinesOfSelectedPaperType",
                    IndexCounter = 0
                };
                db.MSession.Add(newSession);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Globals.logger.LogError(ex.Message);
            }
            finally
            {
                db.Dispose();
            }
        }
    }
}